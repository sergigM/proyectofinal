
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="shortcut icon" type="image/jpg" href="img/logo.png"/>

<!-- BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

<!--JQUERY -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body>

<style>

label {
    display: inline-block;
    font-size: 1.5rem;
    font-family: 'Lato';
}
 
input {
    font-size: 1.5rem;
    font-weight: 100;
    font-family: 'Lato';
}
 
form {
    padding:15px;
    border: 5px solid #ccc;
    width: 500px;


    background-color:white;
    border: 2px solid black;
    border-radius: 10px;
    box-shadow:  0px 0px 5px 1px  grey;
} 
body {
    text-align:center;
    background-color:#D6EEED;
}
</style>
<img src="img/LOGO.svg">
<form method="post" action="loginOperaciones.php" name="signin-form" id="formulario">
    <div class="form-group">
        <label class="form-label">DNI</label>
        <input type="text" class="form-control" name="dni" pattern="[a-zA-Z0-9]+" required />
    </div>
    <div class="form-group">
        <label class="form-label">Contraseña</label>
        <input type="password" class="form-control" name="password" required />
    </div>
    <button class="btn btn-success m-3 " type="submit" name="login" value="login">Conectarse</button>
</form>
<?php
/*
session_start();
if (isset($_SESSION['a']))
    echo $_SESSION['a'];
session_destroy();
*/
?>

</body>
</html>
<script>
$(document).ready(function(){
     $(window).resize(function(){
 
          // aquí le pasamos la clase o id de nuestro div a centrar (en este caso "caja")
          $('#formulario').css({
               position:'absolute',
               left: ($(window).width() - $('#formulario').outerWidth())/2,
               top: ($(window).height() - $('#formulario').outerHeight())/2
          });
          $('img').css({
               position:'absolute',
               left: ($(window).width() - $('img').outerWidth())/2,
               top: ($(window).height() - $('img').outerHeight())/2
          });
  });
// Ejecutamos la función
$(window).resize();
 
});
//http://placeimg.com/640/480/any
</script>
