

<?php
 
include('config/config.php');
session_start();
 
if (isset($_REQUEST['login'])) {
 
    $dni = $_REQUEST['dni'];
    $password = $_REQUEST['password'];
    $db= new DB();
    $query = $db->connect()->prepare("SELECT * FROM Usuarios WHERE DNI=:dni ");
    $query->bindParam(":dni", $dni);
    $query->execute();
 
    $result = $query->fetch(PDO::FETCH_ASSOC);
    if (!$result) {
        echo '<h1 class="error">El dni o la contraseña son incorrectos</h1>';
        /*
        echo '<script>  Swal.fire({
            title: "El dni o la contraseña son incorrectos!",
            icon: "error",
          });</script>';*/
          header('Location: login.php');
    } 
    if($result['rol']=='Cliente' ||  $result['activo']=='0'){
        /*echo '<script>  Swal.fire({
            title: "Usted no tiene permitido el acceso",
            icon: "error",
          });</script>';*/
          echo '<h1 class="error">Usted no tiene permitido el acceso</h1>';
          header('Location: login.php');
        
    }
    else {

        if (hash('sha256',$password) ===$result['contrasena']) {
            $_SESSION['user_id'] = $result['DNI'];
            $_SESSION['rol'] = $result['rol'];
            $apellido=explode(" ",$result['apellido']);
            $_SESSION['nombre']= $result['nombre'] ." ". $apellido[0];
            echo '<p class="success">Te has conectado!</p>';
            header('Location: index.php');
        } else {
            /*
            echo '<script>  Swal.fire({
                title: "El dni o la contraseña son incorrectos!",
                icon: "error",
              });</script>';*/
              echo '<h1 class="error">El dni o la contraseña son incorrectos!</h1>';
              header('Location: login.php');
        }
    }
}
 //https://code.tutsplus.com/es/tutorials/create-a-php-login-form--cms-33261
?>