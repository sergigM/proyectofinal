
<script>
var accioAJAX = new Array()
var capesAJAX = new Array();
capesAJAX['listado'] = '';
capesAJAX['ventana1'] = '';
capesAJAX['ventana2'] = '';

function buidaCapa(nomCapa) {
 capesAJAX[nomCapa] = '';
 $('#' + nomCapa).hide();
 $('#' + nomCapa).html('');

}

function refrescaCapa(nomCapa, setFocus, setSelect) {
 //if(setFocus){ accioAJAX.push(setFocus+'.focus(); '+setFocus+'.select();');}
 if (setFocus) {
     activaFocus(setFocus, setSelect);
 }
 if (capesAJAX[nomCapa]) actualitza(nomCapa, capesAJAX[nomCapa]);

}
module.exports = {
 "buidaCapa": buidaCapa,
 "refrescaCapa":refrescaCapa
}
</script>