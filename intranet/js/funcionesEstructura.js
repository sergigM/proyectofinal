
var accioAJAX = new Array()
var capesAJAX = new Array();
capesAJAX['listado'] = '';
capesAJAX['ventana1'] = '';
capesAJAX['ventana2'] = '';

$(".buttonCerrarVent2").click(function(){
    $("#ventana2").hide();
  });
  
$(".buttonCerrarVent1").click(function(){
    $("#ventana1").hide();
  });

function refrescaCapa(nomCapa, setFocus, setSelect) {
 //if(setFocus){ accioAJAX.push(setFocus+'.focus(); '+setFocus+'.select();');}
 if (setFocus) {
     activaFocus(setFocus, setSelect);
 }
 if (capesAJAX[nomCapa]) actualitza(nomCapa, capesAJAX[nomCapa]);
}

function cierraCapa(nomCapa) {
    capesAJAX[nomCapa] = '';
    $('#' + nomCapa).hide();
    $('#' + nomCapa).html('');
}
function redimensiona(capa,width,height){
  $(capa).css('height', height);
  $(capa).css('width', width);
}
function mostrarAlert(){
  Swal.fire('No se encontraron resultados');
}