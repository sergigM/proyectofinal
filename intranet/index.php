<?php
session_start();
 
if(!isset($_SESSION['user_id'])){
    header('Location: login.php');
    exit;
}  
else {


    // Show users the page!
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dentix</title>
    <link rel="shortcut icon" type="image/jpg" href="img/logo.png"/>

<!-- BOOTSRTAP -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

<!--JQUERY -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link href="./style/estilo.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="./js/funcionesEstructura.js"></script>

<!-- SELECT2 -->
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
  
<!-- SWEET ALERT-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.all.min.js"></script>
<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>

<!--INSTASCAN -->
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<!--FULLCALENDAR -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/es.js"></script>


    <!-- TOGGLE SWITCH-->
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


  <script>
  $( function() {
    $( "#ventana1" ).draggable({ containment: "window",handle:'.header' });
    $( "#ventana2" ).draggable({ containment: "window",handle:'.header' });
    $( "#ventana3" ).draggable({ containment: "window",handle:'.header' });
    $( "#ventana1" ).hide();
    $( "#ventana2" ).hide();
    $( "#ventana3" ).hide();
    $( "#ventanaCarga" ).hide();
    $( "#carga" ).hide();
  } );
  </script>


</head>
<body>

<main class="container">
<div id="menu" style="border-right:3px solid grey;">
<a class="btn show" onclick="(esconder())">
        <i class="fa fa-angle-double-left  text-black"></i>
     </a>
<img src="img/LOGO.svg">
<ul class="list-group list-group-flush">
<li class="list-group-item"><h5><a href="javascript:pantallaCarga();actualitza('ventana2','gestion/fichaje/fichajeFichar.php?tipo=ficharQR');pantallaCarga();"><i class="fas fa-id-card-alt"> </i> Fichar</a></h5></li>
<li class="list-group-item"><h5><a href="javascript:pantallaCarga();actualitza('listado','gestion/usuarios/usuariosLista.php?tipo=clientes');pantallaCarga();"><i class="fas fa-user"> </i> Clientes</a></h5></li>
<li class="list-group-item"><h5><a href="javascript:pantallaCarga();actualitza('listado','gestion/usuarios/usuariosLista.php?tipo=empleados');pantallaCarga();"><i class="fas fa-user"> </i> Empleados</a></h5></li>
<li class="list-group-item"><h5><a href="javascript:pantallaCarga();actualitza('listado','gestion/productos/productosLista.php');pantallaCarga();"><i class="fas fa-boxes"> </i> Productos</a></h5></li>
<li class="list-group-item"><h5><a href="javascript:pantallaCarga();actualitza('listado','gestion/proveedores/proveedoresLista.php');pantallaCarga();"><i class="fas fa-user-tie"> </i> Proveedores</a></h5></li>
<li class="list-group-item"><h5><a href="javascript:pantallaCarga();actualitza('listado','gestion/fichaje/fichajeCalendario.php');pantallaCarga();"><i class="fas fa-calendar"> </i> Calendario</a></h5></li>
</ul>
    </div>
    <div class="body d-flex flex-column m-0">
        
    <div class="header m-2 ">
    <a class="btn btn-outline-secondary hide" onclick="(mostrar())" href="#">
            <i class="fas fa-bars fa-lg"></i>
         </a>
         
    </div>

    <div class="dropdown d-flex justify-content-end" style="margin-right:10px;">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <i class="fas fa-user"></i> <?=  $_SESSION['nombre'] ?>
  </button>
  <ul class="dropdown-menu  " aria-labelledby="dropdownMenuButton1">
    <li><a class="dropdown-item" href="logout.php">Desconexión <i class="fas fa-sign-out-alt"></i></a></li>
  </ul>
</div>
<!-- javascript:pantallaCarga();desconexion();pantallaCarga();-->

    <div id="listado"></div>
    <div id="ventana1" class="ui-widget-content"></div>
    <div id="ventana2" class="ui-widget-content"></div>
    <div id="ventana3" class="ui-widget-content"></div>
    <div id="ventanaCarga" class="ui-widget-content"></div>
    <div id="carga"><button class="btn btn-primary" type="button" disabled>
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  Loading...
</button></div>
   <!-- <button class="boton-top">Vamoh pa arriba</button>-->
</div>
    </main>
<script>
function esconder(){
    var width= $(window).width();
    $('#listado').css('min-width', width-10);
    $('#menu').animate({width:0});
    $('.body').animate({left:0});
    $('.hide').show();
    $('.show').hide();
    $('#menu').hide();
    }

    function mostrar(){
    var width= $(window).width();
    $('#listado').css('min-width', width-200);
    $('#menu').animate({width:500});
    $('.body').animate({left:500});
    $('.show').show();
    $('.hide').hide();
    $('#menu').show();
    }


function actualitza(capa,url,variables){
    $('#'+capa).show();
    $('#'+capa).load(url,variables);
}
$(document).ready(function(){
    var height = $(window).height();
    var width= $(window).width();
    var medioH=height/4;
    var medioW=width/3;

        $("#ventana2").offset({top:medioH,left:medioW});
        $("#ventana3").offset({top:medioH,left:medioW});
        $("#ventana1").offset({top:medioH/2,left:medioW/2});

   
          $('#ventana2').css('width', width/4);
          $('#ventana1').css('height', (height/2)+(height/5));
          $('#ventana1').css('width', width/2+(height/3));


    $('#menu').height(height);
    $('#menu').css('min-width', '200px');
    $('#listado').css('min-width', width-210);
    $('#listado').css('min-height', height-80);

});
function pantallaCarga(){
    $('#carga').toggle(100,function() {

});
}

//FUNCION DE SUBIR PARA ARRIBA Y QUE SE VEA EL BOTON SOLO AL HACER SCROLL
$('.boton-top').click(function(){
    $('body,html').animate({scrollTop : 0}, 500);
    return false;
});
$(window).scroll(function(){
    if ($(this).scrollTop() > 0) {
        $('.boton-top').fadeIn();
    } else {
        $('.boton-top').fadeOut();
    }
});
function desconexion() {

}

    </script>
</body>
</html>