<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
?>
<script type="text/javascript" src="js/funcionesEstructura.js"></script>
<script>
  var height = $(window).height();
  $('tbody').css('max-height', height - 250);
</script>
<?php
//include '../../index.php';
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'productos.class.php';
?>
<div>

  <div class="header d-flex justify-content-end m-2 flex-wrap">
    <button type="button" class="btn btn-outline-primary m-1"
      onclick="pantallaCarga();actualitza('listado','gestion/productos/productosLista.php');pantallaCarga();">Actualiza
      <i class="fas fa-table"></i></button>
    <button type="button" class="btn btn-outline-dark m-1"
      onclick="pantallaCarga();actualitza('ventana2','gestion/productos/productosFicha.php?idProducto=0');pantallaCarga();"><i
        class="fas fa-boxes"></i> </i><i class="fas fa-plus"></i></button>

    <form>
      <div class="form-group m-1">
        <div class="input-group">
          <input type="text" class="form-control inputTamano" id="productoBusqueda" placeholder="Nombre Producto">
          <div class="input-group-append">
            <a class="input-group-text" onclick="pantallaCarga();busquedaProducto();pantallaCarga();"><i
                class="fas fa-search fa-lg "></i></a>
          </div>
        </div>
      </div>
    </form>

  </div>

  <h2 style="text-transform:uppercase;float:left;margin-left:20px;">productos:</h2>
  <div class="body">
    <table class="table table-bordered table-hover ">
      <thead>
        <tr>
          <th class="datosPequenos bg-primary text-white"></th>
          <th class="bg-primary text-white" title="Nombre del producto">Nombre</th>
          <th class="bg-primary text-white" title="Proveedor">Proveedor</th>
          <th class="bg-primary text-white" title="Precio de compra">Precio</th>
          <th class="bg-primary text-white" title="Marca del producto">Marca</th>
          <th class="datosMedianos bg-primary text-white" title="Stock del producto">Stock</th>
          <th class="datosPequenos bg-primary text-white"></th>
        </tr>
      </thead>
      <tbody>
        <?php
$producto = new Producto();
if($_REQUEST['tipo']=="busqueda"){
    $datosProducto= $producto->mostrarProductosFiltrado($_REQUEST['nombreProducto']);
    if($producto->getQuery()=="SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor order by 1 LIMIT 25;"){
        echo '<script>mostrarAlert();</script>';
    }
  }
  if($_REQUEST['tipo']=="next"){
        $datosProducto= $producto->mostrarProductos($_REQUEST['pagina']);
      
}if(!$_REQUEST['tipo']){

    $datosProducto= $producto->mostrarProductos(0);
}
    
foreach ($datosProducto as $fila){
    ?>
        <tr id=<?=$fila['idProducto']?>>
          <td class="datosPequenos">
            <?php
        echo '<style>
        #ventana1{
            overflow-y: scroll;
        }
        </style>';?>
            <a
              href="javascript:pantallaCarga();actualitza('ventana1','gestion/productos/productosFicha.php?idProducto=<?=$fila['idProducto']?>&capa=ventana1');pantallaCarga();"><i
                class="fas fa-edit fa-lg"></i></a>
          </td>
          <td>
            <?=$fila["nombreProducto"]?>
          </td>
          <td>
            <?=$fila['nombreEmpresa']?>
          </td>
          <td>
            <?=$fila['precioCompraProducto']?>€
          </td>
          <td>
            <?=$fila['marcaProducto']?>
          </td>
          <td class="datosMedianos">
            <?=$fila['stockProducto']?>
          </td>
          </td>
          <td class="datosPequenos">
            <?php
        echo '<a href="#" class="text-danger" onClick="javascript:pantallaCarga();eliminar(\''.$fila['idProducto'].'\');pantallaCarga();"><i class="fas fa-trash"></i> </i></a>' ;?>
          </td>
        </tr>
        <?php
              }
              ?>
      </tbody>
    </table>
  <div class="m-2 position-absolute bottom-0 end-0">
      
      <?php 
      $q=$_REQUEST['pagina']-25;
      $e=$_REQUEST['pagina']+25;
if($_REQUEST['tipo']!="busqueda"){
      if($producto->anterior($q)){?>
        <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();anterior();pantallaCarga();"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>

      <?php }  ?>
    
      <?php if($producto->siguiente($e)){?>
      <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();siguiente();pantallaCarga();"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>

      <?php }  }?>
      </div>

      
  </div>
</div>
<!--
<a href="#" id="js_up" class="boton-subir">
    <i class="fa fa-rocket" aria-hidden="true"></i>
</a>
-->
<script>
<?php if (!$_REQUEST['pagina']){
$_REQUEST['pagina']=0;
}; ?>
var a=<?= $_REQUEST['pagina'] ?>  
function siguiente() {
  a = a+25;
  $('#listado').load('gestion/productos/productosLista.php', {
        pagina: a,
        tipo: 'next'
      });
    
  }

  function anterior() {
  a = a-25;
  $('#listado').load('gestion/productos/productosLista.php', {
        pagina: a,
        tipo: 'next'
      });
    
  }

  function redimensiona(capa, width, height) {
    $('#' + capa).css('height', height);
    $('#' + capa).css('width', width);
  }
  function cerrarVentana() {
    $("#ventana1").toggle(1, function () { });
    Swal.fire('No se encontraron resultados')
  }
  function mensajeSinResultados() {
    Swal.fire('No se encontraron resultados')
  }

  function busquedaProducto() {
    if (!$('#productoBusqueda').val()) {
      Swal.fire('Tienes que escribir el nombre de un producto')
    } else {
      //$('#listado').show();
      $('#listado').load('gestion/productos/productosLista.php', {
        nombreProducto: $('#productoBusqueda').val(),
        tipo: 'busqueda'
      });
    }
    <?php
    if ($_REQUEST['capa'] != 'listado') {?>
    
    <?php }   ?>
}
  function eliminar(id) {
    Swal.fire({
      title: "¿Seguro que quieres eliminar este producto?",
      text: "¡Esta operación no se puede deshacer!",
      type: "warning",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "¡Sí,eliminar!",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false,
      reverseButtons: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then((result) => {
      if (result.isConfirmed) {
        $('#' + id).remove();
        $('#ventanaCarga').load('gestion/productos/productosOperaciones.php', {
          idProducto: id,
          operacio: 'eliminar'
        }, function () { //$('#listado').load('gestion/productos/productosLista.php');
         
        });
    <?php
    if ($_REQUEST['capa'] != 'listado') {?>
    
    <?php }   ?>
                    }
    });
  }
</script>