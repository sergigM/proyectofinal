<?php 
include_once 'productos.class.php';
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$operacio=$_REQUEST['operacio'];
switch ($operacio){
 case 'eliminar':
    if($_REQUEST['idProducto'] ){
        $producto = new Producto();
        $producto->setIdProducto($_REQUEST['idProducto']);
        if($idProducto=$producto->eliminarProducto()){
            echo $idProducto;
        }else{
            echo 'ERROR mysql';
        }
    }else{
        echo 'Falta identificador';
    }
  break;

 case 'insertar':
                
        


        $errorProducto=compruebaNombre($_REQUEST['nombreProducto']).compruebaProveedor($_REQUEST['idProveedor']).compruebaMarca($_REQUEST['marcaProducto']).
        validatePrecio($_REQUEST['precioCompraProducto']).validateStock($_REQUEST['stockProducto']);


        if($errorProducto==''){    
            $producto = new Producto();
            $producto->setNombreProducto($_REQUEST['nombreProducto']);
            $producto->setIdProveedor($_REQUEST['idProveedor']);
            $producto->setMarcaProducto($_REQUEST['marcaProducto']);
            $producto->setPrecioCompraProducto($_REQUEST['precioCompraProducto']);
            $producto->setStockProducto($_REQUEST['stockProducto']);
        $idProducto = $producto->insertarProducto();
        if($idProducto){
            echo $idProducto;
        }else{
            echo 'ERROR mysql';
        }
    }else{
        echo $errorProducto;
    }
    
  break;

  case 'modificar':
   if($_REQUEST['idProducto'] ){                
    $producto = new Producto();
    $producto->setIdProducto($_REQUEST['idProducto']);



    switch ($_REQUEST['camp']){
        case 'nombreProducto':
            $error=compruebaNombre($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'idProveedor':
            $error=compruebaProveedor($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'marcaProducto':
            $error=compruebaMarca($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'precioCompraProducto':
            $error=validatePrecio($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'stockProducto':
            $error=validateStock($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'fabricanteProducto':
            $error=compruebaFabricante($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'cbProducto':
            $error=validateCB($_REQUEST['valor']);
            if($error==''){
                $producto->modificarProducto($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;
    }

}else{
    echo 'falta identificador';
}
   break;
   
    default:
    echo 'el campo no existe';
}



//insertar
function compruebaNombre($nombre)
{
    if ($nombre==''){
        return 'El campo NOMBRE está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$nombre)){
        return 'Nombre: '.$nombre . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}

function compruebaProveedor($proveedor)
{
    if(!$proveedor){
        return 'El campo PROVEEDOR está vacio.<br>';
    }
}

function compruebaMarca($marca)
{
    if ($marca==''){
        return 'El campo MARCA está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$marca)){
        return 'Marca: '.$marca . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}

function compruebaFabricante($fabricante)
{
    if ($fabricante==''){
        return 'El campo FABRICANTE está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$fabricante)){
        return 'Fabricante: '.$fabricante . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}


function validatePrecio($precio)
{
   
    if (!$precio) {
        return "El campo PRECIO está vacio.<br>";
    }
  
}

function validateStock($stock)
{
   
    if (!$stock) {
        return "El campo STOCK está vacio.<br>";
    }
  
}


function validateCB($cb)
{
    $nums = str_split($cb);
    if (!$cb) {
        return "El campo CODIGO DE BARRAS está vacio.<br>";
    }if (count($nums)<12||count($nums)>12){
        return "El codigo de barras no consta de 12 numeros.<br>";
    }
  
}


?>