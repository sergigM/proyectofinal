<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
?>
<script type="text/javascript" src="js/funcionesEstructura.js"></script>
<script>
  $(document).ready(function () {
    $(".js-example-basic-single").select2();
  });
</script>
<div>
  <?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//include './index.php';
include_once 'productos.class.php';
$tipoVentana="buttonCerrarVent1";

$idProducto=$_REQUEST['idProducto'];
if( $_REQUEST['cambioClase']){
    $tipoVentana="buttonCerrarVent2";  
}

if($_REQUEST['idProducto']){
   

    $producto =new Producto();

    $result=$producto->mostrarProducto($_REQUEST['idProducto']);
    
        $fila=$result->fetchObject();
        ?>

  <div class="header d-flex justify-content-between m-2">
    <h3 style="text-transform:uppercase;"><?= $fila->nombreProducto ?></h3>
    <button onclick="cierraCapa('<?= $_REQUEST['capa']?>')" class="<?=$tipoVentana ?> buttonCerrar"><i
        class="fas fa-times"></i>Cerrar</button>
  </div>

  <div class="body">

    <div class="card-columns">

      <div class="card m-3">
        <div class="card-header bg-primary text-white">Información producto</div>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-6 ">
              <label>Id del producto</label>
              <input name="idProducto" class="form-control" readonly type="text" value="<?=$fila->idProducto ?>">
            </div>
            <div class="form-group col-6 ">
              <label>Nombre</label>
              <input name="nombreProducto" class="form-control" type="text" value="<?= $fila->nombreProducto ?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-6 ">
              <label>Marca</label>
              <input name="marcaProducto" class="form-control" type="text" value="<?= $fila->marcaProducto ?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-6 ">
              <label>Proveedor</label>
              <input name="nombreEmpresa" class="form-control" type="text" readonly value="<?= $fila->nombreEmpresa?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-6 ">
              <label>Fabricante</label>
              <input name="fabricanteProducto" class="form-control" type="text" value="<?= $fila->fabricanteProducto ?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-6 ">
              <label>Precio de Compra (€)</label>
              <input name="precioCompraProducto" id="precioCompraProducto" class="form-control" type="text"
                value="<?= $fila->precioCompraProducto ?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-6 ">
              <label>Codigo de Barras</label>
              <input name="cbProducto" id="cbProducto" type="text" class="form-control" value="<?= $fila->cbProducto ?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-6 ">
              <label>Stock</label>
              <input name="stockProducto" id="stockProducto" class="form-control" type="text"
                value="<?= $fila->stockProducto ?>"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
            </div>
            <div class="form-group col-12 ">
              <label>Descripción</label>
              <textarea name="descripcionProducto" class="form-control" style="height: 150px"
                onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();"><?= $fila->descripcionProducto?></textarea>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>



  <tr>
  </tr>
  </table>
  <div class="d-flex justify-content-end">
    <?php  echo '<a href="#" class="btn btn-danger text-light m-2" onClick="javascript:pantallaCarga();eliminar(\''.$fila->idProducto.'\');pantallaCarga();">Eliminar <i class="fas fa-trash"></i> </i></a>' ;?>
  </div>
</div>
<?php }else{
 ?>
<div class="header d-flex justify-content-between m-2">
  <h5 style="text-transform:uppercase;">formulario creacion producto</h5>
  <button onclick="cierraCapa('<?= $_REQUEST['capa']?>')" class="buttonCerrarVent2"><i
      class="fas fa-times"></i>Cerrar</button>
</div>
<div class="body">
  <form>
    <div class="row">

      <div class="form-group m-1 col">
        <label>Nombre:</label>
        <input type="text" class="form-control " id="nombreProducto" placeholder="Espejo">
      </div>

      <div class="form-group m-1 col">

        <label>Proveedor:</label>


        <select name="selectProveedores" id="selectProveedores" class="form-control js-example-basic-single">
          <option></option>

          <?php
$producto = new Producto();
$datosProductoEmpresa= $producto->getTodosNombresEmpresa();
foreach ($datosProductoEmpresa as $fila){
    ?>
          <option value="<?=$fila['idProveedor']?>"><?=$fila['nombreEmpresa']?></option>
          <?php
}
?>
        </select>



      </div>

      <div class="form-group m-1 ">
        <label>Marca del producto:</label>
        <input type="text" class="form-control " id="marcaProducto" placeholder="Blizzard" required>
      </div>

      <div class="form-group m-1 ">
        <label>Precio de compra:</label>
        <input class="form-control " id="precioCompraProducto" placeholder="15.35(€)" required>
      </div>
      <div class="form-group m-1 ">
        <label>Stock del producto:</label>
        <input class="form-control " id="stockProducto" placeholder="15" required>
      </div>
      <div class="d-flex justify-content-end  ">
        <button type="button" class="btn btn-outline-primary "
          onclick="pantallaCarga();insertarProducto();pantallaCarga();">Crear</button>
      </div>
    </div>
  </form>
</div>
<?php
}?>









<script>
  setInputFilter(document.getElementById("precioCompraProducto"), function (value) {
    return /^-?\d*[.]?\d*$/.test(value);
  });

  setInputFilter(document.getElementById("stockProducto"), function (value) {
    return /^-?\d*$/.test(value);
  });
  setInputFilter(document.getElementById("cbProducto"), function (value) {
    return /^-?\d*$/.test(value);
  });

  $("#js-example-tags").select2({
    tags: true,
    dropdownParent: $('.header')
  });


  function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
      textbox.addEventListener(event, function () {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
  }


  function modificar(camp, valor) {
    $('#ventana2').load('gestion/productos/productosOperaciones.php', {
      'idProducto': <?= $idProducto ?>,
      'camp': camp,
      'valor': valor,
      'operacio': 'modificar'
    }, (resultat) => {
      if (resultat) {
        Swal.fire({
          title: resultat,
          icon: 'error',
        });

      }
    }
    );
  }

  function insertarProducto() {
    $('#ventanaCarga').load('gestion/productos/productosOperaciones.php', {
      'nombreProducto': $('#nombreProducto').val(),
      'idProveedor': $('#selectProveedores').val(),
      'marcaProducto': $('#marcaProducto').val(),
      'precioCompraProducto': $('#precioCompraProducto').val(),
      'stockProducto': $('#stockProducto').val(),
      'operacio': 'insertar'
    }, (resultat) => {
      if (/[0-9]/.test(resultat.charAt(0))) {
        actulitza(resultat)
      } else {
        Swal.fire({
          title: resultat,
          icon: 'error',
        });
      }
    }
    );
    <?php
    if ($_REQUEST['capa'] != 'listado') {?>
        
    
    <?php }   ?>
}

  function actulitza(resultat) {
    $('#ventana1').load('gestion/productos/productosFicha.php',
      {
        'idProducto': resultat
      });
    $('#ventana1').show();
    $('#ventana2').hide();
  }

  function eliminar(id) {
    Swal.fire({
      title: "¿Seguro que quieres eliminar este producto?",
      text: "¡Esta operación no se puede deshacer!",
      type: "warning",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "¡Sí,eliminar!",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false,
      reverseButtons: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then((result) => {
      if (result.isConfirmed) {
        $('#' + id).remove();
        $('#ventanaCarga').load('gestion/productos/productosOperaciones.php', {
          idProducto: id,
          operacio: 'eliminar'
        }, function () { //$('#listado').load('gestion/productos/productosLista.php');
          $('#' +'<?= $_REQUEST['capa'] ?>').hide();
        });
    <?php
    if ($_REQUEST['capa'] != 'listado') {?>
    
    <?php }   ?>
                    }
    });
  }
</script>