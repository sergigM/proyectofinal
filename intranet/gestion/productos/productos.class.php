<?php
include_once '../../config/config.php';

class Producto{
 private $idProducto;
 private $nombreProducto;
 private $marcaProducto;
 private $fotoProducto;
 private $idProveedor;
 private $fabricanteProducto;
 private $precioCompraProducto;
 private $descripcionProducto;
 private $cbProduto;
 private $stockProducto;

  //VARIABLES EXTRAS
  private $query;
  private $db;

 
public function __construct(){
      $this->$db= new DB();
}
public function getIdProducto(){
    return $this->idProducto;
}

public function setIdProducto($idProducto){
    $this->idProducto = $idProducto;
}

public function getNombreProducto(){
    return $this->nombreProducto;
}

public function setNombreProducto($nombreProducto){
    $this->nombreProducto = $nombreProducto;
}

public function getMarcaProducto(){
    return $this->marcaProducto;
}

public function setMarcaProducto($marcaProducto){
    $this->marcaProducto = $marcaProducto;
}

public function getFotoProducto(){
    return $this->fotoProducto;
}

public function setFotoProducto($fotoProducto){
    $this->fotoProducto = $fotoProducto;
}

public function getIdProveedor(){
    return $this->idProveedor;
}

public function setIdProveedor($idProveedor){
    $this->idProveedor = $idProveedor;
}

public function getFabricanteProducto(){
    return $this->fabricanteProducto;
}

public function setFabricanteProducto($fabricanteProducto){
    $this->fabricanteProducto = $fabricanteProducto;
}

public function getPrecioCompraProducto(){
    return $this->precioCompraProducto;
}

public function setPrecioCompraProducto($precioCompraProducto){
    $this->precioCompraProducto = $precioCompraProducto;
}

public function getDescripcionProducto(){
    return $this->descripcionProducto;
}

public function setDescripcionProducto($descripcionProducto){
    $this->descripcionProducto = $descripcionProducto;
}

public function getCbProduto(){
    return $this->cbProduto;
}

public function setCbProduto($cbProduto){
    $this->cbProduto = $cbProduto;
}

public function getStockProducto(){
    return $this->stockProducto;
}

public function setStockProducto($stockProducto){-
    $this->stockProducto = $stockProducto;
}
public function getQuery(){
    return $this->query;
}



function siguiente($e){    
    $query =$this->$db->connect()->query("SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor order by 1 LIMIT " . $e . ",25;");
    if($query->rowCount()==0){
        return;
    }
    return $query;
}

function anterior($q){    
    $query =$this->$db->connect()->query("SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor order by 1 LIMIT " . $q . ",25;");
    return $query;
}


function mostrarProductos($a){    
    $query =$this->$db->connect()->query("SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor order by 1 LIMIT " . $a . ",25;");
    return $query;
}

function mostrarProducto($idProducto){
    
    $query =$this->$db->connect()->query("SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor WHERE idProducto=".$idProducto.' limit 1');
    return $query;
}

function mostrarProductosFiltrado($nombreProducto){
    $query =$this->$db->connect()->query("SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor where nombreProducto like '%" . $nombreProducto . "%';");
    if($query->rowCount()==0){
        $this->query="SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor order by 1 LIMIT 25;";
        $query =$this->$db->connect()->query("SELECT * FROM Productos INNER JOIN Proveedores ON Productos.idProveedor=Proveedores.idProveedor order by 1 LIMIT 25;");
    }
    return $query;
}

function getTodosNombresEmpresa(){
    $query =$this->$db->connect()->query("SELECT nombreEmpresa,idProveedor FROM `Proveedores` where Activo=1");
    return $query;
}


function modificarProducto($camp,$valor){
    $query = $this->$db->connect()->query("UPDATE Productos SET " . $camp . "='" . addslashes($valor) . "' WHERE idProducto='" . $this->idProducto . "';");
       return $query;
}
function insertarProducto(){

    $db=$this->$db->connect();
   
    $query =$db->prepare("INSERT INTO Productos (nombreProducto,idProveedor,marcaProducto,precioCompraProducto,stockProducto) VALUES (:nombreProducto,:idProveedor,:marcaProducto,:precioCompraProducto,:stockProducto);");
    $query->bindParam(":nombreProducto",addslashes($this->nombreProducto));
    $query->bindParam(":idProveedor",addslashes($this->idProveedor));
    $query->bindParam(":marcaProducto",addslashes($this->marcaProducto));
    $query->bindParam(":precioCompraProducto",addslashes($this->precioCompraProducto) );
    $query->bindParam(":stockProducto",addslashes($this->stockProducto));
    $db->beginTransaction();
    $query->execute();
    $id=$db->lastInsertId();
        $db->commit();    
        return $id;
}
function eliminarProducto(){
    $query = "DELETE from Productos WHERE idProducto='" . $this->idProducto . "';";
    if ($this->$db->connect()->query($query)) {
        return $db->thread_id;
    }
    return false;
}
}


?>