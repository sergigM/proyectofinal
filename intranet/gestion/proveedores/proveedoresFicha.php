<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
?>
<script type="text/javascript" src="js/funcionesEstructura.js"></script>

<div>
<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//include './index.php';
include_once 'proveedores.class.php';
$idProveedor=$_REQUEST['idProveedor'];
if($_REQUEST['idProveedor']){
    $proveedor =new Proveedor();
    //print_r($_REQUEST['idProveedor']);
    $result=$proveedor->mostrarProveedor($_REQUEST['idProveedor']);
    
    
        $fila=$result->fetchObject();
        ?>

        <div class="header d-flex justify-content-between m-2">
        <h3 style="text-transform:uppercase;"  ><?= $fila->nombreEmpresa ?></h3>
        <button class="buttonCerrar " onclick="cierraCapa('<?= $_REQUEST['capa']?>')"><i class="fas fa-times"></i>Cerrar</button>
        </div>

<div class="body">

<div class="card-columns">
    
    <div class="card m-3">
    <div class="card-header bg-primary text-white">Datos proveedor</div>
    <div class="card-body">
    <div class="row">
    <div class="col-8 border-end border-2 border-primary" >
    <div class="row">

    <div class="form-group col-6 ">
    <label>IdProveedor</label> 
    <input name="idProveedor" class="form-control" readonly type="text" value="<?= $fila->idProveedor; ?>">
	</div>

  <div class="form-group col-6 ">
    <label>Nombre proveedor</label> 
    <input name="nombreEmpresa" class="form-control" type="text" value="<?= $fila->nombreEmpresa; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

  <div class="form-group col-6 ">
  <label> Nombre del contacto </label>
  <input name="nombreContacto" class="form-control" type="text" value="<?= $fila->nombreContacto;?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

  <div class="form-group col-6 ">
  <label> Cargo del contacto </label>
  <input name="cargoContacto" class="form-control" type="text" value="<?= $fila->cargoContacto; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

  <div class="form-group col-6 ">
  <label> Telefono del contacto </label>
  <input name="telefonoContacto" id="telefonoContacto" class="form-control" type="text" value="<?= $fila->telefonoContacto;?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

  <div class="form-group col-6 ">
  <label> Email del contacto </label>
  <input name="emailContacto" class="form-control" type="text" value="<?= $fila->emailContacto;?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

  
</div>
    </div>
    <div class="col-4">
    <div class="form-group ">
    <label> Ciudad del proveedor </label>
    <input name="ciudadProveedor" class="form-control" type="text" value="<?= $fila->ciudadProveedor; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

  <div class="form-group ">
  <label> Codigo postal del Proveedor </label>
  <input name="codigoPostalProveedor"  id="codigoPostalProveedor" class="form-control" type="text" value="<?= $fila->codigoPostalProveedor; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div> 

  <div class="form-group">
        <label>Activo </label>
        <div>
        <?php
        echo '<input type="checkbox" class="toggle-switch" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger" data-size="sm"';
         if($fila->Activo){
            echo ' checked="checked"';
         }
         echo ' onchange="javascript:pantallaCarga();eliminar(\''.$fila->idProveedor.'\');pantallaCarga();">';
         ?>
            </div>
            </div>

    </div>

    </div>


    </div>
    </div>
    </div>


    <div class="card m-3">
<div class="card-header bg-primary text-white">Productos de este proveedor</div>
<div class="card-body">
<table class="table table-bordered table-hover " >
 <thead >
<tr>
<th class="datosPequenos bg-primary text-white"></th>
<th class="bg-primary text-white" title="Nombre del producto">Nombre</th>
<th class="bg-primary text-white" title="Proveedor">Proveedor</th>
<th class="bg-primary text-white" title="Precio de compra">Precio</th>
<th class="bg-primary text-white" title="Marca del producto">Marca</th>
<th class="datosMedianos bg-primary text-white" title="Stock del producto">Stock</th>
<th class="datosPequenos bg-primary text-white"></th>
</tr>
 </thead>
 <tbody>
<?php 
$datosProductos=$proveedor->productosRelacionados($_REQUEST['idProveedor']); 
foreach ($datosProductos as $fila){
  ?>
 
 <tr id=<?=$fila['idProducto']?>>
    <td class="datosPequenos" >
    <?php
        echo '<style>
        #ventana1{
            overflow-y: scroll;
        }
        </style>';?>
        <a href="javascript:pantallaCarga();actualitza('ventana2','gestion/productos/productosFicha.php?idProducto=<?=$fila['idProducto']?>&capa=ventana2');pantallaCarga();"><i class="fas fa-edit fa-lg"></i></a>
        </td>
        <td>
            <?=$fila["nombreProducto"]?>
        </td>
        <td>
            <?=$fila['nombreEmpresa']?>
        </td>
        <td>
            <?=$fila['precioCompraProducto']?>€
        </td>
        <td>
            <?=$fila['marcaProducto']?>
        </td>
        <td class="datosMedianos">
            <?=$fila['stockProducto']?>
        </td>
        </td>
        <td class="datosPequenos"><?php
        echo '<a href="#" class="text-danger" onClick="javascript:pantallaCarga();eliminarCita(\''.$fila['idProducto'].'\');pantallaCarga();"><i class="fas fa-trash"></i> </i></a>' ;?>
        </td>
    </tr>
    <?php
}
?>
</tbody>
</table>  

</div>
</div>

    </div>

        </div>
<?php }else{
 ?>
<div class="header d-flex justify-content-between m-2">
        <h5 style="text-transform:uppercase;" >formulario creacion proveedor</h5>
        <button class="buttonCerrar" onclick="cierraCapa('<?= $_REQUEST['capa']?>')"><i class="fas fa-times"></i>Cerrar</button>
        </div>
<div class="body">
<form>
<div class="row">

<div class="form-group m-1 col">
<label>Nombre:</label>
      <input type="text" class="form-control " id="nombreEmpresa" placeholder="Industras Pepe S.L.">
  </div>

  <div class="form-group m-1 col">
<label>Contacto:</label>

<input type="text" class="form-control " id="nombreContacto" placeholder="Juan Escobar">

  </div>

  <div class="form-group m-1 col">
<label>Cargo:</label>

<input type="text" class="form-control " id="cargoContacto" placeholder="Director">

  </div>

  <div class="form-group m-1 ">
<label>Email de contacto:</label>
      <input type="email" class="form-control " id="emailContacto" placeholder="ejemplo@ejemplo.com" required>
  </div>

  <div class="form-group m-1 ">
<label>Telefono de contacto:</label>

      <input type="text" class="form-control " id="telefonoContacto" placeholder="657890234" required>
  </div>
<div class="d-flex justify-content-end">
<button  type="button" class="btn btn-outline-primary" onclick="pantallaCarga();insertarProveedor();pantallaCarga();">Crear</button>
</div>
  </div>
</form>
</div>

 <?php
}?>
</div>


<script>
$('.toggle-switch').bootstrapToggle();

setInputFilter(document.getElementById("telefonoContacto"), function(value) {
  return /^-?\d*$/.test(value); });


setInputFilter(document.getElementById("codigoPostalProveedor"), function(value) {
  return /^-?\d*$/.test(value); });



function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}





function modificar(camp,valor){
        $('#ventana2').load('gestion/proveedores/proveedoresOperaciones.php',{
            'idProveedor': <?= $idProveedor ?>,
            'camp': camp,
            'valor': valor,
            'operacio':'modificar'
        }, (resultat) =>{ 
        if(resultat){
          Swal.fire({
                      title: resultat,
                      icon: 'error',
                    });
            
        } 
    }
    );
        
}

function insertarProveedor(){
        $('#ventanaCarga').load('gestion/proveedores/proveedoresOperaciones.php',{
        'nombreEmpresa': $('#nombreEmpresa').val(),
        'nombreContacto':$('#nombreContacto').val(),
        'cargoContacto':$('#cargoContacto').val(),
        'emailContacto': $('#emailContacto').val(),
        'telefonoContacto': $('#telefonoContacto').val(),
        'operacio':'insertar'
    }, (resultat) =>{ 
        if (/[0-9]/.test(resultat.charAt(0))){
            actulitza(resultat)
            }else{
              Swal.fire({
                      title: resultat,
                      icon: 'error',
                    });
                //Swal.fire({html: resultat,width: '1000px'});
            }
        } 
        
    );
    <?php
    if($_REQUEST['capa']!='listado'){?>
        
    
    <?php }   ?>
}

function actulitza(resultat){
    $('#ventana1').load('gestion/proveedores/proveedoresFicha.php',
    {
        'idProveedor':resultat,
        'capa':'ventana1'
    }); 
    $('#ventana1').show();
    $('#ventana2').hide();
    }
</script>