<?php 
include_once 'proveedores.class.php';

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$operacio=$_REQUEST['operacio'];
switch ($operacio){
 case 'eliminar':
    
    if($_REQUEST['idProveedor'] ){
        $proveedor = new Proveedor();
        $proveedor->setIdProveedor($_REQUEST['idProveedor']);
        if($idProveedor=$proveedor->eliminarProveedor()){
            //echo $idProveedor;
        }else{
            echo 'ERROR mysql';
        }
    }else{
        echo 'Falta identificador';
    }

  break;

 case 'insertar':
       $errorProveedor=compruebaNombre($_REQUEST['nombreEmpresa']).compruebaNombreContacto($_REQUEST['nombreContacto']).validateTel($_REQUEST['telefonoContacto']).
        compruebaCargo($_REQUEST['cargoContacto']).comprobarEmail($_REQUEST['emailContacto']);

if($errorProveedor==''){
    $proveedor = new Proveedor();
    $proveedor->setNombreEmpresa($_REQUEST['nombreEmpresa']);
    $proveedor->setnombreContacto($_REQUEST['nombreContacto']);
    $proveedor->setCargoContacto($_REQUEST['cargoContacto']);
    $proveedor->setEmailContacto($_REQUEST['emailContacto']);
    $proveedor->setTelefonoContacto($_REQUEST['telefonoContacto']);
    $idProveedor=$proveedor->insertarProveedor();
       
       
            if($idProveedor){
                echo $idProveedor;
            }else{
                echo 'ERROR mysql';
            }
}else{
    echo $errorProveedor;
}
       break;

  case 'modificar':
   if($_REQUEST['idProveedor'] ){                
    $proveedor = new Proveedor();
    $proveedor->setIdProveedor($_REQUEST['idProveedor']);

    switch ($_REQUEST['camp']){
        case 'nombreEmpresa':
            $error=compruebaNombre($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'nombreContacto':
            $error=compruebaNombreContacto($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'telefonoContacto':
            $error=validateTel($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'emailContacto':
            $error=comprobarEmail($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'cargoContacto':
            $error=compruebaCargo($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'ciudadProveedor':
            $error=compruebaCiudad($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;

        case 'codigoPostalProveedor':
            $error=comprobarCodigoPostal($_REQUEST['valor']);
            if($error==''){
                $proveedor->modificarProveedor($_REQUEST['camp'],$_REQUEST ['valor']);
            }else{
                echo $error;
            }
        break;
    }

    }else{
        echo 'falta identificador';
    }
   break;

}




function compruebaCodigoPostal($cargoContacto)
{
    if ($cargoContacto==''){
        return 'El campo CODIGO POSTAL está vacio.<br>';
    }
    return '';
}

function compruebaCiudad($nombre)
{
    if ($nombre==''){
        return 'El campo CIUDAD PROVEEDOR está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$nombre)){
        return 'Ciudad: '.$nombre . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}

function compruebaNombre($nombre)
{
    if ($nombre==''){
        return 'El campo NOMBRE está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$nombre)){
        return 'Nombre: '.$nombre . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}

function compruebaCargo($cargoContacto)
{
    if ($cargoContacto==''){
        return 'El campo CARGO DEL CONTACTO está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$cargoContacto)){
        return 'Cargo: '.$cargoContacto . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}

function compruebaNombreContacto($nombre)
{
    if ($nombre==''){
        return 'El campo NOMBRE CONTACTO está vacio.<br>';
    }
    if ( !preg_match("/^[a-zA-Z ]+$/",$nombre)){
        return 'Nombre del contacto: '.$nombre . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}


function validateTel($tel)
{
    $numsTel = str_split($tel);
    if ($tel == '') {
        return "El campo TELEFONO está vacio.<br>";
    }
    if (count($numsTel) != 9 || $numsTel[0] != 6 && $numsTel[0] != 7) {
        return "Telefono no valido.<br>";
    }
    return '';
}

function comprobarEmail($mail){
    if ($mail==''){
        return 'El campo EMAIL DE CONTACTO está vacio.<br>';
    }
    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        return "Email no válido.<br>";
    }
    return '';
}
function comprobarCodigoPostal($cp){
    if ($cp==''){
        return 'El campo CODIGO POSTAL está vacio.<br>';
    }
    if (!preg_match("/^[0-9]+$/",$cp)||strlen($cp)!=5) {
        return "Codigo postal no válido.<br>";
    }
    return '';
}





?>