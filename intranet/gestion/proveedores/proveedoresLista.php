<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
?>
<script type="text/javascript" src="js/funcionesEstructura.js"></script>
<script>
var height = $(window).height();
$('tbody').css('max-height', height-250);
</script>
<?php
//include '../../index.php';
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'proveedores.class.php';
?>
<div>
<?php  
if(!$_REQUEST['nombreEmpresa']){?>
<div class="header d-flex justify-content-end m-2 flex-wrap">
<button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();actualitza('listado','gestion/proveedores/proveedoresLista.php');pantallaCarga();">Actualiza <i class="fas fa-table"></i></button>  
<button type="button" class="btn btn-outline-dark m-1" onclick="pantallaCarga();actualitza('ventana2','gestion/proveedores/proveedoresFicha.php?idProveedor=0&capa=ventana2');pantallaCarga();"><i class="fas fa-user-tie"></i> </i><i class="fas fa-plus"></i></button>

<form>
<div class="form-group m-1">
    <div class="input-group">
      <input type="text" class="form-control inputTamano" id="proveedorBusqueda" placeholder="Nombre Proveedor">
      <div class="input-group-append">
      <a class="input-group-text" onclick="pantallaCarga();busquedaProveedor();pantallaCarga();"><i class="fas fa-search fa-lg "></i></a>
      </div>
    </div>
  </div>
</form>
<?php }if($_REQUEST['nombreEmpresa']){?>
    <div class="header d-flex justify-content-between m-2">
    <h3>Lista productos:</h3>
    <button class="buttonCerrarVent1"><i class="fas fa-times"></i>Cerrar</button>
    <?php 
}
echo '</div>';
if(!$_REQUEST['nombreEmpresa']){
 echo '<h2 style="text-transform:uppercase;float:left;margin-left:20px;">proveedores:</h2>';
} ?>

<div class="body">
<table class="table table-bordered table-hover " >
 <thead >
<tr>
<th class="datosPequenos bg-primary text-white"></th>
<th class="bg-primary text-white" title="Nombre del proveedor">Proveedor</th>
<th class="bg-primary text-white" title="Nombre del contacto" >Contacto</th>
<th class="bg-primary text-white" title="Cargo del contacto">Cargo</th>
<th class="bg-primary text-white" title="Telefono del Contacto" >Telefono</th>
<th class="bg-primary text-white" title="Correo del contacto" >Correo </th>
<th class="bg-primary text-white" title="Ciudad del proveedor">Ciudad</th>
<th class="bg-primary text-white" title="Codigo postal del proveedor">Codigo Postal</th>
<th class="bg-primary text-white datosMedianos" title="Activo">Activo</th>
</tr>
 </thead>
 <tbody>
<?php

$proveedor = new Proveedor();
if($_REQUEST['tipo']=="busqueda"){
    $datosProveedor= $proveedor-> mostrarProveedoresFiltrado($_REQUEST['nombreProveedor']);
    if($proveedor->getQuery()=="SELECT * FROM Proveedores order by 1 LIMIT 25;"){
        echo '<script>mostrarAlert();</script>';
    }
}
if($_REQUEST['tipo']=="next"){
    $datosProveedor= $proveedor->mostrarProveedores($_REQUEST['pagina']);
  
}if(!$_REQUEST['tipo']){
    $datosProveedor= $proveedor->mostrarProveedores(0);
}

//$datosProveedor= $proveedor->mostrarProveedores();
foreach ($datosProveedor as $fila){
    ?>
   <tr>
    <td class="datosPequenos">
        <a href="javascript:pantallaCarga();actualitza('ventana1','gestion/proveedores/proveedoresFicha.php?idProveedor=<?=$fila['idProveedor']?>&capa=ventana1');pantallaCarga();"><i class="fas fa-edit fa-lg"></i></a>
        </td>
        <td>
            <?=$fila["nombreEmpresa"]?>
        </td>
        <td>
            <?=$fila['nombreContacto']?>
        </td>
        <td>
            <?=$fila['cargoContacto']?>
        </td>
        <td>
            <?=$fila['telefonoContacto']?>
        </td>
        <td style="overflow: hidden;text-overflow: ellipsis; white-space: nowrap;" title="<?=$fila['emailContacto']?>">
            <?=$fila['emailContacto']?>
        </td>
        <td>
            <?=$fila['ciudadProveedor']?>
        </td>
        <td>
            <?=$fila['codigoPostalProveedor']?>
        </td>
        <td class="datosMedianos">
        <?php
        echo '<input type="checkbox" class="toggle-switch" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger" data-size="sm"';
         if($fila['Activo']){
            echo ' checked="checked"';
         }
         echo ' onchange="javascript:pantallaCarga();eliminar(\''.$fila['idProveedor'].'\');pantallaCarga();">';
         ?>
        </td>
        
    </tr>
    <?php
}
?>
</tbody>
</table>  
<div class="m-2 position-absolute bottom-0 end-0">
<?php 
if($_REQUEST['tipo']!="busqueda"){
      $q=$_REQUEST['pagina']-25;
      $e=$_REQUEST['pagina']+25;

      if($proveedor->anterior($q)){?>
        <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();anterior();pantallaCarga();"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>

      <?php }  ?>
    
      <?php if($proveedor->siguiente($e)){?>
        <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();siguiente();pantallaCarga();"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>

      <?php }}  ?>
    
      </div>

</div>
</div>
<!--
<a href="#" id="js_up" class="boton-subir">
    <i class="fa fa-rocket" aria-hidden="true"></i>
</a>
-->
<script>

<?php if (!$_REQUEST['pagina']){
$_REQUEST['pagina']=0;
}; ?>
var a=<?= $_REQUEST['pagina'] ?>  
function siguiente() {
  a = a+25;
  $('#listado').load('gestion/proveedores/proveedoresLista.php', {
        pagina: a,
        tipo: 'next'
      });
    
  }

  function anterior() {
  a = a-25;
  $('#listado').load('gestion/proveedores/proveedoresLista.php', {
        pagina: a,
        tipo: 'next'
      });
    
  }




$('.toggle-switch').bootstrapToggle();
function busquedaProveedor(){
    if(!$('#proveedorBusqueda').val()){
        Swal.fire('Tienes que escribir el nombre de un proveedor')
    }else{
        $('#listado').load('gestion/proveedores/proveedoresLista.php',{
        nombreProveedor: $('#proveedorBusqueda').val(),
        tipo:"busqueda"
    });
    }
    <?php
    if($_REQUEST['capa']!='listado'){?>
    
    <?php }   ?>
}
function eliminar(id){
    $('#ventanaCarga').load('gestion/proveedores/proveedoresOperaciones.php',{
        idProveedor: id,
        operacio:'eliminar'
    });
}

function eliminarCita(id){
    Swal.fire({
                    title: "¿Seguro que quieres eliminar este producto?",
                    text: "¡Esta operación no se puede deshacer!",
                    type: "warning",
                    icon:"warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Sí,eliminar!",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    reverseButtons: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                }).then((result) => {
                    if (result.isConfirmed) {

                        $('#'+id).remove();
                        $('#ventana2').load('gestion/productos/productosOperaciones.php',{
        idProducto: id,
        operacio:'eliminar'
    },function (){ //$('#listado').load('gestion/productos/productosLista.php');
    $('#ventana2').hide();});
    <?php
    if($_REQUEST['capa']!='listado'){?>
    
    <?php }   ?>
                    }
                });
}
</script>