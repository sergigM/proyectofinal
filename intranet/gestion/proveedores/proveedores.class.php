<?php
include_once '../../config/config.php';
class Proveedor{
 private $idProveedor;
 private $nombreEmpresa;
 private $telefonoContacto;
 private $nombreContacto;
 private $cargoContacto;
 private $emailContacto;
 private $ciudadProveedor;
 private $codigoPostalProveedor;

  //VARIABLES EXTRAS
  private $query;
  private $db;

 
public function __construct(){
    $this->$db= new DB();
}
public function getIdProveedor(){
    return $this->idProveedor;
}

public function setIdProveedor($idProveedor){
    $this->idProveedor = $idProveedor;
}

public function getNombreEmpresa(){
    return $this->nombreEmpresa;
}

public function setNombreEmpresa($nombreEmpresa){
    $this->nombreEmpresa = $nombreEmpresa;
}

public function getTelefonoContacto(){
    return $this->telefonoContacto;
}

public function setTelefonoContacto($telefonoContacto){
    $this->telefonoContacto = $telefonoContacto;
}

public function getNombreContacto(){
    return $this->nombreContacto;
}

public function setNombreContacto($nombreContacto){
    $this->nombreContacto = $nombreContacto;
}

public function getCargoContacto(){
    return $this->cargoContacto;
}

public function setCargoContacto($cargoContacto){
    $this->cargoContacto = $cargoContacto;
}

public function getEmailContacto(){
    return $this->emailContacto;
}

public function setEmailContacto($emailContacto){
    $this->emailContacto = $emailContacto;
}

public function getCiudadProveedor(){
    return $this->ciudadProveedor;
}

public function setCiudadProveedor($ciudadProveedor){
    $this->ciudadProveedor = $ciudadProveedor;
}

public function getCodigoPostalProveedor(){
    return $this->codigoPostalProveedor;
}

public function setCodigoPostalProveedor($codigoPostalProveedor){
    $this->codigoPostalProveedor = $codigoPostalProveedor;
}
public function getQuery(){
    return $this->query;
}


function siguiente($e){    
    $query =$this->$db->connect()->query("SELECT * FROM Proveedores order by 1 LIMIT " . $e . ",25;");
    if($query->rowCount()==0){
        return;
    }
    return $query;
}

function anterior($q){    
    $query =$this->$db->connect()->query("SELECT * FROM Proveedores order by 1 LIMIT " . $q . ",25;");
    return $query;
}


public function mostrarProveedores($q){
    $query =$this->$db->connect()->query("SELECT * FROM Proveedores order by 1 LIMIT " . $q . ",25;");
    return $query;
}

public function mostrarProveedor($idProveedor){
    $query =$this->$db->connect()->query("SELECT * FROM Proveedores where idProveedor='" . $idProveedor . "';");
    return $query;
}

function mostrarProveedoresFiltrado($nombreEmpresa){
    $query =$this->$db->connect()->query("SELECT * FROM Proveedores where nombreEmpresa like '%" . $nombreEmpresa . "%';");
    if($query->rowCount()==0){
        $this->query="SELECT * FROM Proveedores order by 1 LIMIT 25;";
        $query =$this->$db->connect()->query("SELECT * FROM Proveedores order by 1 LIMIT 25;");
    }
    return $query;
}
function modificarProveedor($camp,$valor){
    $query = $this->$db->connect()->query("UPDATE Proveedores SET " . $camp . "='" . addslashes($valor) . "' WHERE idProveedor='" . $this->idProveedor . "';");
    return $query;

}
function productosRelacionados($idProveedor){
    $query=$this->$db->connect()->query("SELECT * FROM Productos where idProveedor ='" .$idProveedor . "'");
    return $query;
}


function insertarProveedor(){

    $db=$this->$db->connect();
    $query =$db->prepare("INSERT INTO Proveedores (nombreEmpresa,nombreContacto,cargoContacto,emailContacto,telefonoContacto) VALUES (:nombreEmpresa,:nombreContacto,:cargoContacto,:emailContacto,:telefonoContacto);");
    $query->bindParam(":nombreEmpresa",addslashes($this->nombreEmpresa));
    $query->bindParam(":nombreContacto",addslashes($this->nombreContacto) );
    $query->bindParam(":cargoContacto",addslashes($this->cargoContacto));
    $query->bindParam(":emailContacto", addslashes($this->emailContacto));
    $query->bindParam(":telefonoContacto",addslashes($this->telefonoContacto));
    $db->beginTransaction();
    $query->execute();
    $id=$db->lastInsertId();
        $db->commit();    
        return $id;
}


function eliminarProveedor(){
        $query = $this->$db->connect()->query("UPDATE Proveedores set Activo=1  where idProveedor='" . $this->idProveedor . "';");
   if($query->rowCount()==0){
    $query = $this->$db->connect()->query("UPDATE Proveedores set Activo=0  where idProveedor='" . $this->idProveedor . "';");
   }
    
    
    return $query;

}


/*
public function mostrarProveedorNombre($nombreEmpresa){
    $db=new DB();
    $query =$db->connect()->query("SELECT * FROM Proveedores where idProveedor='" . $nombreEmpresa . "'order by 1 LIMIT 19;");
    return $query;
}

*/


}

?>