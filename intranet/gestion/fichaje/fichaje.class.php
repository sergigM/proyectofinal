<?php
include_once '../../config/config.php';
class Fichaje{
 private $idFichaje;
 private $horaFichaje;
 private $idUsuario;

 
public function __construct(){

}
public function getIdFichaje(){
    return $this->idFichaje;
}

public function setIdFichaje($idFichaje){
    $this->idFichaje = $idFichaje;
}

public function getHoraFichaje(){
    return $this->horaFichaje;
}

public function setHoraFichaje($horaFichaje){
    $this->horaFichaje = $horaFichaje;
}

public function getIdUsuario(){
    return $this->idUsuario;
}

public function setIdUsuario($idUsuario){
    $this->idUsuario = $idUsuario;
}
function cargarCitas(){
    $db=new DB();
        $query =$db->connect()->query('SELECT * FROM `Usuarios` INNER join `Fichajes` on Usuarios.idUsuario= Fichajes.idUsuario  where Fichajes.tipo="Cita" ORDER BY horaFichaje DESC');
        if($query->rowCount()==0){
            return false;
     }
    return $query;
}
}

?>