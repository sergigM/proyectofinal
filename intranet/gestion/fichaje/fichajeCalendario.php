<script type="text/javascript" src="js/funcionesEstructura.js"></script>
<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'fichaje.class.php';
$fichajes=new Fichaje();
$citas= $fichajes->cargarCitas();
?>

<div>
<div class="header">
</div>
<div class="body">
<div id='calendar'></div>

</div>
</div>
<style>
.basura:hover{
  color: red;
}
</style>
<script>
$(document).ready(function() {
    var altura = $(window).height()-200;
    var ancho = $(window).width()-200;
  $('#calendar').fullCalendar({
    contentHeight: altura,
    width:ancho,
    header: {
        left: 'prev,next,today,prevYear,nextYear',
        center: 'title',
        right: 'month,listWeek'
    },
    navLinks: true,
    editable: true,
    eventLimit: 1, 
    themeSystem: 'bootstrap4',
    //themeName:'Material',


        buttonIcons: {next: 'right-single-arrow', prevYear: 'left-double-arrow', nextYear: 'right-double-arrow'},
        //POPUP
        eventRender: function(event, element,view){
          element.popover({
              animation:true,
              delay: 300,
              title:event.title+"",
              html:true,
              content:"Fecha:"+moment(event.start).format("DD-MM-YYYY HH:mm:ss")+"<br>"+event.description,
              placement: 'left',
              trigger: 'hover',
          });
           if (view.name == 'listDay') {
            element.find(".fc-list-item-time").append("<span class='closeon'><i class='fas fa-trash basura'></i></span>");
        } else {
            element.find(".fc-content").prepend("<span class='closeon'><i class='fas fa-trash basura'></i></span>");
        }
        element.find(".closeon").on('click', function() {
            eliminarCita(event.id);
            });
        },
        /*
        customButtons: {
    addEventButton: {
      text: 'CREAR CITA',
      click: function () {
        var dateStr = prompt('Enter date in YYYY-MM-DD format');
        var date = moment(dateStr);

        if (date.isValid()) {
          $('#calendar').fullCalendar('renderEvent', {
            title: 'Cita de Roberto',
            start: date,
            allDay: true,
          });
        } else {
          alert('Invalid Date');
        }

      }
    }citas->num_rows!=0
  },*/
  dayClick: function (date, jsEvent, view) {
    var date = date.format();
    Swal.fire({
    title: "Cita",
    html:
            '<label>Hora</label>'+
            '<input type="time" class="swal2-input" id="hora" name="hora" value="11:45:00" max="22:30:00" min="10:00:00" step="1">'+
            '<label>DNI</label>'+
            '<input id="dni" class="swal2-input" placeholder="57834565H">',
    showCancelButton: true,  
}).then((result) => {
  if (result.isConfirmed) {
    var dni=$('#dni').val();
      var hora=$('#hora').val();
      if(!dni || !hora){
        Swal.fire({
        icon: 'error',
        title: 'Alguno de los datos esta vacio',
      });
      }else{
    var fecha=date+" "+hora;
    pantallaCarga();crearCita(fecha,dni,date,hora);pantallaCarga();
      }
    }
});
  },
  events: [
      <?php if($citas->rowCount()!=0){
        foreach ($citas as $fila){
          $fecha=explode(" ",$fila['horaFichaje']);?>
      {
          html:true,
            title:'Cita de <?= $fila['nombre'] ?> <?= $fila['apellido'] ?>',
            start:'<?= $fecha[0]."T".$fecha[1]."Z"?>',
            id:'<?=$fila['idFichaje']?>',    
            description: 'Usuario: <?= $fila['nombre']?>  <?= $fila['apellido']?> <br> Telefono: <?= $fila['telefono']?> <br> DNI: <?= $fila['DNI']?>'
        },
        <?php   
      }
      }?>
        
    ]
  
  });
});

function crearCita(fecha,dni,date,hora){
        $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
        'fecha': fecha,
        'dni':dni,
        'operacio':'crearCita',
      }, (resultat) =>{ if(resultat=="1"){
        nuevaCitaCalendar(date,dni,hora);
        Swal.fire({
                      title: 'Nueva cita agregada',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 1500
                    });
        actualiza();}
        if(resultat==0){
          Swal.fire({
                text: "No se pudo enviar el correo, revise que el email del usuario es correcto.",
                title:'Nueva cita agregada',
              });
              actualiza();
        }}
    );
    }
    function nuevaCitaCalendar(date,dni,hora){
      $('#calendar').fullCalendar('renderEvent', {
        title: 'Cita de '+dni,
        start: date+"T"+hora+"Z",
      });
    }
    function actualiza(){
            $('#listado').load('gestion/fichaje/fichajeCalendario.php'); 
        }
        function eliminarCita(id){

 Swal.fire({
      title: "¿Seguro que quieres eliminar esta cita?",
      text: "¡Esta operación no se puede deshacer!",
      type: "warning",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "¡Sí,eliminar!",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false,
      reverseButtons: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then((result) => {
      if (result.isConfirmed) {  
        $('#calendar').fullCalendar('removeEvents',id);
 $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
        'id': id,
        'operacio':'eliminarCita',
    });
  
                    }
    });

        }
    </script>
<!-- 
  inputValidator: nombre => {
      console.log(hora+" dasdsd");
            // Si el valor es válido, debes regresar undefined. Si no, una cadena
            if (!dni) {
                return "Por favor escribe el dni";
            } 
            if (!hora) {
                return "Por favor escribe la hora";
            }else {
                return undefined;
            }
        }
-->