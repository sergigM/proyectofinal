<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
include_once 'usuario.class.php';
?>
<?php
$usuario = new Usuario();?>
<script type="text/javascript" src="js/funcionesEstructura.js"></script>
<script>function cerrarVentana1(){
    $( "#ventana1").hide();
    Swal.fire('No se encontraron resultados')
}</script>
<div>
<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//include '../../index.php';
$condicion=true;
$idUsuario=$_REQUEST['idUsuario'];
    /*
    $datosUsuarioDNI= $usuario->mostrarUsuarioFiltrado($_REQUEST['dniUsuario']);
    
    $fila2=$datosUsuarioDNI->fetchObject();
    if($fila2->idUsuario){
        $idUsuario= $fila2->idUsuario;
    }
        */

    if($idUsuario){ 
        $datosUsuario=$usuario->abrirFichaUsuario($idUsuario);
            $fila=$datosUsuario->fetchObject();
            ?>
            <div class="header d-flex justify-content-between m-2">
            <h3 style="text-transform:uppercase;"  ><?= $fila->nombre ?> <?= $fila->apellido ?> - <?=$fila->rol ?></h3>
            <button class="buttonCerrar" onclick="cierraCapa('<?= $_REQUEST['capa']?>')"><i class="fas fa-times"></i>Cerrar</button>
            </div>
    
    <div class="body">

    <div class="card-columns">
    
    <div class="card m-3">
    <div class="card-header bg-primary text-white">Datos personales</div>
    <div class="card-body">
    <div class="row">
    <div class="col-8 border-end border-2 border-primary">
    <div class="row">
    <div class="form-group col-12 ">
    <label>IdUsuario</label> 
    <input name="idUsuario" class="form-control" readonly type="text" value="<?= $fila->idUsuario; ?>">
	</div>

    <div class="form-group col-6 ">
    <label>Nombre</label>
    <input name="nombre" class="form-control"  type="text" value="<?= $fila->nombre; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

    <div class="form-group col-6 ">
    <label>Apellido</label> 
    <input name="apellido" class="form-control"  type="text" value="<?= $fila->apellido; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

    <div class="form-group col-6 ">
    <label>Telefono</label>  
    <input name="telefono"  id="telefonoUsuario" class="form-control"  type="text" value="<?= $fila->telefono; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

    <div class="form-group col-6 ">
    <label>Email</label>
    <input name="email" class="form-control"  type="text" value="<?= $fila->email; ?>" onchange="pantallaCarga();modificar(this.name,this.value);pantallaCarga();">
	</div>

    <div class="form-group col-6 ">
    <label>DNI</label>
    <input name="DNI" class="form-control" readonly type="text" value="<?= $fila->DNI; ?>">
	</div>

    <div class="form-group col-6 ">
    <label>Rol</label>
    <?php if( $_SESSION['rol'] =="Admin" ||  $_SESSION['rol'] == "Jefe" ){?>
    <div class="input-group">
    <select id="rolUsuarioFicha" class="form-control">
      <option value="Jefe" <?php if($fila->rol=="Jefe") echo "selected"?>>Jefe</option>
      <option value="Admin" <?php if($fila->rol=="Admin") echo "selected"?>>Admin</option>
      <option value="Cliente" <?php if($fila->rol=="Cliente") echo "selected"?>>Cliente</option>
      <option value="Empleado" <?php if($fila->rol=="Empleado") echo "selected"?>>Empleado</option>
      </select>
      <div class="input-group-append">
      <button type="button" class="btn btn-outline-secondary form-control " onclick="pantallaCarga();modificar('rol',$('#rolUsuarioFicha').val());pantallaCarga();">Cambiar rol</button> 
      </div>
      </div>
      <?php }else{ ?>
        <input name="rol" class="form-control"  type="text" readonly value="<?= $fila->rol; ?>" >
      <?php }?>


	</div>
</div>

</div>


<div class="col-4 ">

    <div class="form-group ">
    <label>Contraseña</label>
    <button style="margin-bottom:20px;" type="button" class="btn btn-outline-secondary form-control " onclick="pantallaCarga();resetearPass('resetear');pantallaCarga();">Resetear contraseña</button> 
    <button style="margin-bottom:20px;" type="button" class="btn btn-outline-secondary form-control " onclick="pantallaCarga();resetearPass('cambiar');pantallaCarga();">Cambiar contraseña</button>
	</div>

    <div class="form-group ">
    <label>Activo</label>
    <div>
    <?php
        echo '<input type="checkbox" class="toggle-switch form-control m-2" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger" data-size="sm"';
         if($fila->activo){
            echo ' checked="checked"';
         }
         if(($fila->rol =="Admin" || $fila->rol =="Jefe") && $_SESSION['rol']=="Empleado"){

         }else{
         echo ' onchange="javascript:pantallaCarga();eliminar(\''.$fila->idUsuario.'\');pantallaCarga();">';
         }
         ?>
         </div>
	</div>

</div>


</div>

            </div>
</div>
<?php 
if($_REQUEST['tabla']=="tablaFichajes" && $_REQUEST['tabla']!="tablaCitas"){
    $datosCitas=$usuario->obtenerCitasFiltrado($idUsuario,$_REQUEST['fecha1F'],$_REQUEST['fecha2F']);
}else{
    $datosCitas=$usuario->obtenerHorario($idUsuario,"fichajes");
}
if($fila->rol!="Cliente"){?>
<div class="card m-3">
<div class="card-header bg-primary text-white">Tabla fichajes</div>
<div class="card-body">
<form>
<div class="row">
    
    <div class="form-group m-1 col">
    <label>Primera fecha:</label>
          <input type="date" class="form-control " id="fecha1F"  required value="<?=$_REQUEST['fecha1F']?>">
      </div>
    
      <div class="form-group m-1 col">
    <label>Segunda fecha:</label>
          <input type="date" class="form-control" id="fecha2F"  required value="<?=$_REQUEST['fecha2F']?>">
      </div>
      <div class="form-group m-1 col">
      <button type="button" class="btn btn-outline-secondary" onclick="pantallaCarga();cargarCitas('tablaFichajes','<?= $_REQUEST['capa']?>');pantallaCarga();">Filtrar</button> 
      </div>
      
      </div>
</form>
<table class="table table-bordered table-hover" id="tablaFichajes">
<thead>
<tr>
<th class="bg-info">Dia </th>
<th class="bg-info">Hora</th>
</tr>
</thead>
<tbdody style="max-height:250px !important;">
<?php foreach ($datosCitas as $hora){
    if($hora['tipo']=="Fichaje"){
        $dia=explode(" ",$hora['horaFichaje']);
    ?>
<tr>
<td>
<?= $dia[0]?>
</td>
<?php
echo '<td>';

echo $dia[1];
echo '</td>';
}
?>
</tr>

<?php }?>
</tbody>
</table>
</div>
</div>
<?php }
if($_REQUEST['tabla']=="tablaCitas" && $_REQUEST['tabla']!="tablaFichajes"){
    $datosCitas=$usuario->obtenerCitasFiltrado($idUsuario,$_REQUEST['fecha1'],$_REQUEST['fecha2']);
}else{
 $datosCitas=$usuario->obtenerHorario($idUsuario,"citas");
}?>


<div class="card m-3">
<div class="card-header bg-primary text-white">Tabla citas</div>
<div class="card-body">
<form>
<div class="row">
    
    <div class="form-group m-1 col">
    <label>Primera fecha:</label>
          <input type="date" class="form-control " id="fecha1"  required value="<?=$_REQUEST['fecha1']?>">
      </div>
    
      <div class="form-group m-1 col">
    <label>Segunda fecha:</label>
          <input type="date" class="form-control" id="fecha2"  required value="<?=$_REQUEST['fecha2']?>">
      </div>
      <div class="form-group m-1 col">
      <button type="button" class="btn btn-outline-secondary" onclick="pantallaCarga();cargarCitas('tablaCitas','<?= $_REQUEST['capa']?>');pantallaCarga();">Filtrar</button> 
      </div>
      
      </div>
</form>
<table class="table table-bordered table-hover" id="tablaCitas">
<thead>
<tr>
<th class="bg-info">Dia </th>
<th class="bg-info">Hora</th>
<th class="bg-info datosPequenos"></th>
</tr>
</thead>
<tbody  style="max-height:250px !important;">
<?php foreach ($datosCitas as $hora){
    if($hora['tipo']=="Cita"){
        $dia=explode(" ",$hora['horaFichaje']);
    ?>
<tr id="<?=$hora['idFichaje']?>">
<td>
<?= $dia[0]?>
</td>
<?php
echo '<td>';

echo $dia[1];
echo '</td>';

echo '<td class="datosPequenos">';
echo '<a href="#" class="text-danger" onClick="javascript:pantallaCarga();eliminarCita(\''.$hora['idFichaje'].'\');pantallaCarga();"><i class="fas fa-trash"></i> </i></a>' ;
echo '</td>';
}
?>
</tr>

<?php }?>
</tbody>
</table>

</div>
</div>

            </div>
            </div>
            <?php }else{
     ?>
    <div class="header d-flex justify-content-between m-2">
            <h5 style="text-transform:uppercase;" >formulario creacion usuario</h5>
            <button class="buttonCerrar" onclick="cierraCapa('<?= $_REQUEST['capa']?>')"><i class="fas fa-times"></i>Cerrar</button>
            </div>
    <div class="body">



    <form>
    <div class="row">
    
    <div class="form-group m-1 col">
    <label>Nombre:</label>
          <input type="text" class="form-control " id="nombreUsuario" placeholder="Carlos" required>
      </div>
    
      <div class="form-group m-1 col">
    <label>Apellido:</label>
          <input type="text" class="form-control" id="apellidoUsuario" placeholder="Garriga" required>
      </div>
    
      <div class="form-group m-1 ">
    <label>DNI:</label>
          <input type="text" class="form-control " id="dniUsuario" placeholder="DNI" required>
      </div>
    
      <div class="form-group m-1 ">
    <label>Telefono:</label>
          <input type="text" class="form-control " id="telefonoUsuario" placeholder="657890234" required>
      </div>
      
      
      <div class="form-group m-1">
      <label>Rol </label>
      <select id="rolUsuario" class="form-control">
      <?php if( $_SESSION['rol'] =="Admin" ||  $_SESSION['rol'] == "Jefe" ){?>
      <option value="Jefe">Jefe</option>
      <option value="Admin">Admin</option>
      <?php }?>
      <option value="Cliente">Cliente</option>
      <option value="Empleado">Empleado</option>
      </select>
      </div>
    <div class="d-flex justify-content-end ">
    <button type="button" class="btn btn-outline-primary" onclick="pantallaCarga();insertarUsuario();pantallaCarga();">Crear usuario</button>
    </div>

      </div>
    </form>
    </div>
     <?php
    }?>

    </div>
<script>
function resetearPass(tipo){
    if(tipo=="resetear"){

        Swal.fire({
                    title: "¿Seguro que quieres resetear la contraseña?",
                    type: "warning",
                    icon:"warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Sí,resetea!",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    reverseButtons: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
                        idUsuario:"<?= $idUsuario?>",
                        tipo:tipo,
                        operacio:'cambiarPassword'
                        });
                        Swal.fire({
                      title: 'Contraseña cambiada',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 1500
                    });
                    }
                });

    }else{
        $('#ventana3').show();
        $('#ventana3').load('gestion/usuarios/cambioPassword.php',{
            idUsuario:"<?=$idUsuario?>"
        });
    }

}

   // $("#"+tabla+" > tbody").empty();

function cargarCitas(tabla,capa){
    
    if(tabla=="tablaFichajes"){
        if(!$('#fecha1F').val() || !$('#fecha2F').val()){
        Swal.fire('Tienes que escribir ambas fechas');
    }else{
        $('#ventana1').load('gestion/usuarios/usuariosFicha.php',{
        tabla: tabla,
       fecha1F:$('#fecha1F').val(),
       fecha2F:$('#fecha2F').val(),
       capa:capa,
       idUsuario:'<?=$idUsuario?>'
    });
    }
    }else{
        if(!$('#fecha1').val() || !$('#fecha2').val()){
        Swal.fire('Tienes que escribir ambas fechas');
    }else{
    //$('#ventana1').show();
        $('#ventana1').load('gestion/usuarios/usuariosFicha.php',{
        tabla: tabla,
       fecha1:$('#fecha1').val(),
       fecha2:$('#fecha2').val(),
       capa:capa,
       idUsuario:'<?=$idUsuario?>'
    });
    }
    }
    
}


$('.toggle-switch').bootstrapToggle();

setInputFilter(document.getElementById("telefonoUsuario"), function(value) {
  return /^-?\d*$/.test(value); });



function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}




function insertarUsuario(){
        $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
        'nombreUsuario': $('#nombreUsuario').val(),
        'apellidoUsuario': $('#apellidoUsuario').val(),
        'dniUsuario': $('#dniUsuario').val(),
        'telefonoUsuario': $('#telefonoUsuario').val(),
        'rolUsuario':$('#rolUsuario').val(),
        'operacio':'insertar'
    }, (resultat) =>{ 
        
        if (/[0-9]/.test(resultat.charAt(0))){
            actulitza(resultat);
            }else{
                Swal.fire({
                      title: resultat,
                      icon: 'error',
                    });
            }
        } 
    );
    <?php
    if($_REQUEST['capa']!='listado'){?>
        
    
    <?php }   ?>
}
    function eliminarCita(id){
        $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
        'id': id,
        'operacio':'eliminarCita',
    });
    $("#"+id).remove();
    }


function modificar(camp,valor){
        $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
        'idUsuario': <?= $idUsuario ?>,
        'camp': camp,
        'valor': valor,
        'operacio':'modificar'
    }, (resultat) =>{ 
        if(resultat){
            Swal.fire({
                      title: resultat,
                      icon: 'error',
                    });
            
        } 
    }
    );
   
}

function actulitza(resultat){
            $('#ventana1').load('gestion/usuarios/usuariosFicha.php',
            {
                'idUsuario':resultat
            }); 
            $('#ventana1').show();
            $('#ventana2').hide();
        }

        function eliminar(id){
    
    $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
    idUsuario: id,
    operacio:'eliminar'
});

            
}
</script>