<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
?>

    <script type="text/javascript" src="js/funcionesEstructura.js"></script>
    <script>
var height = $(window).height();
$('tbody').css('max-height', height-250);
</script>
<?php
//include '../../index.php';
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include_once 'usuario.class.php';
$tipo=$_REQUEST['tipo'];
?>
<div>

<div class="header d-flex justify-content-end m-2 flex-wrap">
<button type="button" class="btn btn-outline-dark m-1" onclick="pantallaCarga();actualitza('ventana2','gestion/fichaje/fichajeFichar.php?tipo=ficha');pantallaCarga();">Ficha con QR <i class="fas fa-camera"></i></button>
<button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();actualitza('listado','gestion/usuarios/usuariosLista.php?tipo=<?php if(isset($_REQUEST['tipoUser'])){echo $_REQUEST['tipoUser'];}else{echo $tipo;}?>');pantallaCarga();">Actualiza <i class="fas fa-table"></i></button>
<button type="button" class="btn btn-outline-dark m-1" onclick="pantallaCarga();actualitza('ventana2','gestion/usuarios/usuariosFicha.php?idUsuario=0&capa=ventana2');pantallaCarga();"><i class="fas fa-user-plus"></i></button>

<form>
<div class="form-group m-1">
    <div class="input-group">
      <input type="text" class="form-control inputTamano" id="dniBusqueda" placeholder="DNI o nombre y apellido" required>
      <div class="input-group-append">
      <a class="input-group-text" id="buscar" onclick="pantallaCarga();busquedaUsuario();pantallaCarga();"><i class="fas fa-search fa-lg "></i></a>
      </div>
    </div>
  </div>
</form>
</div>
<?php
if($tipo=="clientes"){
 echo '<h2 style="text-transform:uppercase;float:left;margin-left:20px;">clientes:</h2>';
}
if($tipo=="empleados"){
 echo '<h2 style="text-transform:uppercase;float:left;margin-left:20px;">empleados:</h2>';
}
if($tipo=="busqueda"){
 echo '<h2 style="text-transform:uppercase;float:left;margin-left:20px;">busqueda usuarios:</h2>';
}
?>
<div class="body">
<table class="table table-bordered table-hover ">
 <thead >
<tr class="bg-primary text-white">
<th class="datosPequenos bg-primary text-white"></th>
<th class="bg-primary text-white">Nombre</th>
<th class="bg-primary text-white">Apellido</th>
<th class="bg-primary text-white">Telefono</th>
<th class="bg-primary text-white">Email</th>
<th class="datosMedianos bg-primary text-white">Activo</th>

</tr>
 </thead>
 <tbody>
<?php
$user = new Usuario();
if($tipo=="clientes"){
 $datosUser= $user->mostrarClientes(0);

 if($_REQUEST['tipoPagina']=="nextCliente"){
    $datosUser= $user->mostrarClientes($_REQUEST['pagina']); 
}

}
if($tipo=="empleados"){
 $datosUser= $user->mostrarEmpleados(0);
}
if($tipo=="busqueda"){
    $datosUser= $user->busquedaUsuarioFiltrado($_REQUEST['dniUsuario']);
    if($user->getQuery()=="SELECT * FROM Usuarios order by 1 LIMIT 25;"){
        echo '<script>mostrarAlert();</script>';
    }
}

foreach ($datosUser as $fila){
    ?>
    <td class="datosPequenos">
        <a href="javascript:actualitza('ventana1','gestion/usuarios/usuariosFicha.php?idUsuario=<?=$fila['idUsuario']?>&capa=ventana1')"><i class="fas fa-edit fa-lg"></i></a>
        </td>
        <td>
            <?=$fila["nombre"]?>
        </td>
        <td>
            <?=$fila['apellido']?>
        </td>
        <td>
            <?=$fila['telefono']?>
        </td>
        <td  style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;" title=<?=$fila['email']?>>
            <?=$fila['email']?>
        </td>
        <td class="datosMedianos">
        <?php
        echo '<input type="checkbox" class="toggle-switch form-control m-2" data-toggle="toggle" data-on="SI" data-off="NO" data-onstyle="success" data-offstyle="danger" data-size="sm"';
         if($fila['activo']){
            echo ' checked="checked"';
         }
         if(($fila['rol'] =="Admin" || $fila['rol']  =="Jefe") && $_SESSION['rol']=="Empleado"){

         }else{
         echo ' onchange="javascript:pantallaCarga();eliminar(\''.$fila['idUsuario'].'\');pantallaCarga();">';
         }
         ?>
        </td>

        
       
 
    </tr>
    <?php
}
?>
</tbody>
</table>
<div class="m-2 position-absolute bottom-0 end-0">
<?php 
$q=$_REQUEST['pagina']-25;
$e=$_REQUEST['pagina']+25;
if($tipo=="clientes"){
    if($user->anteriorCliente($q)){?>
        <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();anteriorCliente();pantallaCarga();"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>

      <?php }  ?>
    
      <?php if($user->siguienteCliente($e)){?>
      <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();siguienteCliente();pantallaCarga();"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>

      <?php }
    }else{  ?>
    <?php 
if($user->anteriorEmpleado($q)){?>
    <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();anteriorEmpleado();pantallaCarga();"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>

  <?php }  ?>

  <?php if($user->siguienteEmpleado($e)){?>
  <button type="button" class="btn btn-outline-primary m-1" onclick="pantallaCarga();siguienteEmpleado();pantallaCarga();"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>

  <?php }}

      ?>
   
   </div>

     
</div>
</div>
<!--
<a href="#" id="js_up" class="boton-subir">
    <i class="fa fa-rocket" aria-hidden="true"></i>
</a>
-->
<script>
//paginado
<?php if (!$_REQUEST['pagina']){
$_REQUEST['pagina']=0;
}; ?>
var a=<?= $_REQUEST['pagina'] ?>  
function siguienteCliente() {
  a = a+25;
  $('#listado').load('gestion/usuarios/usuariosLista.php', {
        pagina: a,
        tipoPagina: 'nextCliente',
        tipo: '<?=$tipo ?>'
      });
    
  }

  function anteriorCliente() {
  a = a-25;
  $('#listado').load('gestion/usuarios/usuariosLista.php', {
        pagina: a,
        tipoPagina: 'nextCliente',
        tipo: '<?=$tipo ?>'
      });
    
  }
  
  function siguienteEmpleado() {
  a = a+18;
  $('#listado').load('gestion/usuarios/usuariosLista.php', {
        pagina: a,
        tipoPagina: 'nextEmpleado',
        tipo: '<?=$tipo ?>'
      });
    
  }

  function anteriorEmpleado() {
  a = a-18;
  $('#listado').load('gestion/usuarios/usuariosLista.php', {
        pagina: a,
        tipoPagina: 'nextEmpleado',
        tipo: '<?=$tipo ?>'
      });
    
  }
  
  



/* busqueda con ENTER
$("#dniBusqueda").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#buscar").click();
    }
});
*/
$('.toggle-switch').bootstrapToggle();
function busquedaUsuario(){
    if(!$('#dniBusqueda').val()){
        Swal.fire('Tienes que escribir o dni o nombre y apellido del usuario');
    }else{
    //$('#ventana1').show();
        $('#listado').load('gestion/usuarios/usuariosLista.php',{
        dniUsuario: $('#dniBusqueda').val(),
        cambioClase:"cambio",
        tipo:"busqueda",
        tipoUser:'<?= $tipo?>'
    });
    }
    <?php
    if($_REQUEST['capa']!='listado'){?>
    
    <?php }   ?>
}

function eliminar(id){
    
        $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
        idUsuario: id,
        operacio:'eliminar'
    });
    
                
}
</script>
