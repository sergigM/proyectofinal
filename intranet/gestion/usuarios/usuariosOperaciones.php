<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include_once 'usuario.class.php';
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$operacio=$_REQUEST['operacio'];
switch ($operacio){

case "cambiarPassword":
    if($_REQUEST['idUsuario'] && $_REQUEST['tipo']){
        $usuario = new Usuario();
        $usuario->setIdUsuario($_REQUEST['idUsuario']);
        if($_REQUEST['tipo']=="resetear"){
            $idUsuario=$usuario->resetearPassword("","");
        }else{
            $idUsuario=$usuario->resetearPassword($_REQUEST['oldPassword'],$_REQUEST['newPassword']);
        }
        
        if($idUsuario){
            echo "1";
        }else{
            echo '0';
        }
    }else{
        echo 'Falta identificador';
    }
    break;

 case 'eliminar':
    if($_REQUEST['idUsuario'] ){
        $usuario = new Usuario();
        $usuario->setIdUsuario($_REQUEST['idUsuario']);
        $idUsuario=$usuario->eliminarUsuario();
        if($idUsuario){
            //echo $idUsuario;
        }else{
            echo 'ERROR mysql';
        }
    }else{
        echo 'Falta identificador';
    }
  break;
  case 'eliminarCita':
    if($_REQUEST['id'] ){
        echo $_REQUEST['id'] ;
        $usuario = new Usuario();
        $idCita=$usuario->eliminarCita($_REQUEST['id']);
        if($idCita){
            //echo $idUsuario;
        }else{
            echo 'ERROR mysql';
        }
    }else{
        echo 'Falta identificador';
    }
  break;

 case 'insertar':
        $errorUsuario=compruebaNombre($_REQUEST['nombreUsuario']).compruebaApellido($_REQUEST['apellidoUsuario']).validateTel($_REQUEST['telefonoUsuario']).
        compruebaDni($_REQUEST['dniUsuario']);


        if($errorUsuario==''){     
            $usuario = new Usuario();
            $usuario->setNombre($_REQUEST['nombreUsuario']);
            $usuario->setApellido($_REQUEST['apellidoUsuario']);
            $usuario->setDNI($_REQUEST['dniUsuario']);
            $usuario->setTelefono($_REQUEST['telefonoUsuario']);
            $usuario->setRol($_REQUEST['rolUsuario']);
              
  
        $idUsuario=$usuario->insertarUsuario();
        if($idUsuario){
            echo $idUsuario;
        }else{
            echo 'El usuario ya existe';
        }
    }else{
        echo $errorUsuario;
    }
    
  break;

  case 'modificar':
   if($_REQUEST['idUsuario'] ){                
    $usuario = new Usuario();
    $usuario->setIdUsuario($_REQUEST['idUsuario']);

        switch ($_REQUEST['camp']){
            case 'nombre':
                $error=compruebaNombre($_REQUEST['valor']);
                if($error==''){
                    $usuario->modificarUsuario($_REQUEST['camp'],$_REQUEST ['valor']);
                }else{
                    echo $error;
                }
            break;

            case 'apellido':
                $error=compruebaApellido($_REQUEST['valor']);
                if($error==''){
                    $usuario->modificarUsuario($_REQUEST['camp'],$_REQUEST ['valor']);
                }else{
                    echo $error;
                }
            break;

            case 'telefono':
                $error=validateTel($_REQUEST['valor']);
                if($error==''){
                    $usuario->modificarUsuario($_REQUEST['camp'],$_REQUEST ['valor']);
                }else{
                    echo $error;
                }
            break;

            case 'email':
                $error=comprobarEmail($_REQUEST['valor']);
                if($error==''){
                    $usuario->modificarUsuario($_REQUEST['camp'],$_REQUEST ['valor']);
                }else{
                    echo $error;
                }
            break;
            case 'rol':
                if($error==''){
                    $usuario->modificarUsuario($_REQUEST['camp'],$_REQUEST ['valor']);
                }else{
                    echo $error;
                }
            break;
        }


        
    }else{
        echo 'Falta identificador';
    }
   break;



   case 'crearCita':
    if($_REQUEST['dni'] && $_REQUEST['fecha']){
        $usuario= new Usuario();
        if($datosUser=$usuario->mostrarUsuarioFiltrado($_REQUEST['dni'])){
        $fila=$datosUser->fetchObject();
        $idUsuario=$fila->idUsuario;
        if($usuario->citaUsusario($idUsuario,$_REQUEST['fecha'])){

            require '../../Exception.php';
            require '../../PHPMailer.php';
            require '../../SMTP.php';


            $mail = new PHPMailer(true);
            $emailUser=$fila->email;

            try {
                //Server settings
                $mail->SMTPDebug = 0;                      //Enable verbose debug output
                $mail->isSMTP();                                            //Send using SMTP
                $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                $mail->Username   = 'carlos.plaza.lendinez@gmail.com';                     //SMTP username
                $mail->Password   = 'Gy81y0p@';                               //SMTP password
                $mail->SMTPSecure = 'tls';         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                //Recipients
                $mail->setFrom('carlos.plaza.7e3@itb.cat', 'Dentix');
                $mail->addAddress($emailUser);     //Add a recipient
            

                //Content
                $mail->isHTML(true);                                  //Set email format to HTML
                $mail->Subject = 'Cita dentista';
                $mail->Body    = '<html> 
                <head> 
                <title>Correo para citas</title> 
                </head> 
                <body> 
                <h1>Buenos dias estimado cliente '. $fila->nombre.' '. $fila->apellido.',</h1> 
                <p> 
                Este correo es un mensaje recordatorio de que tiene cita en el dentista con Dentix el dia '.$_REQUEST['fecha'] .'.Si hubiera un problema agradeceriamos que contactara con nosotros para informarnos.
                </p> 
                <p>
                Un saludo,
                </p>
                <p>
                Atentamente Dentix.
                </p>
                </body> 
                </html> 
                ';

                $mail->send();
                echo '1';
        
            } catch (Exception $e) {
                echo '0';
                //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }
           
            
                
        }else{
            echo '<script>
            Swal.fire({
                icon: "error",
                title: "El dni no existe",
              });
            </script>';
        }
    }
    }else{
        echo '<script>
            Swal.fire({
                icon: "error",
                title: "El dni no existe",
              });
            </script>';
    }
    break;
    
    
   /*
   case 'busquedaEmpleado':
    if($_REQUEST['dniUsuario'] ){
        $usuario = new Usuario();
        $usuario->setDNI($_REQUEST['dniUsuario']);
        if($dniUsuario=$usuario->mostrarEmpleadoFiltrado()){
            echo 'funciona';
        }else{
            echo 'ERROR mysql';
        }
    }else{
        echo 'Falta identificador';
    }
    break;*/
   
    default:
    echo 'El campo no existe';
}






//insertar
function compruebaNombre($nombre)
{
    if ($nombre==''){
        return 'El campo NOMBRE está vacio.<br>';
    }
    if ( !preg_match("/[a-zA-Z ]+/",$nombre)){
        return 'Nombre: '.$nombre . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}

function compruebaDni($dni)
{
    if(!$dni){
        return 'El campo DNI está vacio.<br>';
    }
    if(!is_string($dni[0])){
        return 'Campo DNI no es válidooooooo.<br>';
    }
    $letra = substr($dni, -1);
    $letra=strtoupper($letra);
    $numeros = substr($dni, 0, -1);
    if(!$numeros){
        $numeros=0;
    }
    if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
        return '';
    }else{
        return 'Campo DNI no es válido.<br>';
    }
}

function compruebaApellido($nombre)
{
    if ($nombre==''){
        return 'El campo APELLIDOS está vacio.<br>';
    }
    if ( !preg_match("/[a-zA-Z ]+/",$nombre)){
        return 'Apellido/s: '.$nombre . ' no es válido, sólo se admiten carácteres y espacios.<br>';
    }
    return '';
}


function validateTel($tel)
{
    $numsTel = str_split($tel);
    if ($tel == '') {
        return "El campo TELEFONO está vacio.<br>";
    }
    if (count($numsTel) != 9 || $numsTel[0] != 6 && $numsTel[0] != 7) {
        return "Telefono no valido.<br>";
    }
}


//modificar
function comprobarEmail($mail){
    if ($mail == '') {
        return "El campo EMAIL está vacio.<br>";
    }
    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        return "Email no válido.<br>";
    }
}



?>