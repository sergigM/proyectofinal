<?php
include_once '../../config/config.php';

class Usuario{
 private $idUsuario;
 private $nombre;
 private $apellido;
 private $DNI;
 private $email;
 private $contrasena;
 private $telefono;
 private $rol;
 private $activo;

 //VARIABLES EXTRAS
 private $query;
 private $db;

 
public function __construct(){
    $this->$db= new DB();
}
public function getIdUsuario(){
    return $this->idUsuario;
}

public function setIdUsuario($idUsuario){
    $this->idUsuario = $idUsuario;
}

public function getNombre(){
    return $this->nombre;
}

public function setNombre($nombre){
    $this->nombre = $nombre;
}

public function getApellido(){
    return $this->apellido;
}

public function setApellido($apellido){
    $this->apellido = $apellido;
}

public function getDNI(){
    return $this->DNI;
}

public function setDNI($DNI){
    $this->DNI = $DNI;
}

public function getEmail(){
    return $this->email;
}

public function setEmail($email){
    $this->email = $email;
}

public function getContrasena(){
    return $this->contrasena;
}

public function setContrasena($contrasena){
    $this->contrasena = $contrasena;
}

public function getTelefono(){
    return $this->telefono;
}

public function setTelefono($telefono){
    $this->telefono = $telefono;
}

public function getRol(){
    return $this->rol;
}

public function setRol($rol){
    $this->rol = $rol;
}

public function getActivo(){
    return $this->activo;
}

public function setActivo($activo){
    $this->activo = $activo;
}
public function getQuery(){
    return $this->query;
}



function siguienteCliente($e){    
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where rol='Cliente' order by idUsuario LIMIT " . $e . ",25;");
    if($query->rowCount()==0){
        return;
    }
    return $query;
}

function anteriorCliente($q){    
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where rol='Cliente' order by idUsuario LIMIT " . $q . ",25;");
    return $query;
}


function siguienteEmpleado($e){    
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where rol='Admin' or rol='Empleado' or rol='Jefe' order by idUsuario LIMIT " . $e . ",25;");
    if($query->rowCount()==0){
        return;
    }
    return $query;
}

function anteriorEmpleado($q){    
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where rol='Admin' or rol='Empleado' or rol='Jefe' order by idUsuario LIMIT " . $q . ",25;");
    return $query;
}




function mostrarClientes($q){
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where rol='Cliente' order by idUsuario LIMIT " . $q . ",25;");
    return $query;
}
function mostrarEmpleados($q){

    
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where rol='Admin' or rol='Empleado' or rol='Jefe' LIMIT " . $q . ",25;");
    return $query;
}
function mostrarUsuarioFiltrado($dniUsuario){
    /*
    $db=new DB();
    $query =$db->connect()->query("SELECT * FROM Usuarios where DNI='" . $dniUsuario . "';");
    return $query;*/

    $query =$this->$db->connect()->query("SELECT * FROM Usuarios  where DNI like '%" . $dniUsuario . "%';");
    if($query->rowCount()==0){
        $this->query="SELECT * FROM Usuarios order by 1 LIMIT 25;";
        $query =$this->$db->connect()->query("SELECT * FROM Usuarios order by 1 LIMIT 25;");
    }
    return $query;
}
function busquedaUsuarioFiltrado($variable){
    $datos=explode(" ",$variable);


    if(is_numeric($datos[0][0])){
        $query =$this->$db->connect()->query("SELECT * FROM Usuarios  where DNI like '%" . $datos[0] . "%';");
    }else{
if(is_string($datos[0][0]) && count($datos)==1){
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios  where nombre like '%" . $datos[0] . "%' or apellido like '%" . $datos[0] . "%';");
}
if(is_string($datos[0][0]) && count($datos)==3){
    $apellido= $datos[1] ." ".$datos[2];
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios  where nombre like '%" . $datos[0] . "%' and apellido like '%" . $apellido . "%';");
}
if(is_string($datos[0][0]) && count($datos)==2){
    $query =$this->$db->connect()->query("SELECT * FROM Usuarios  where nombre like '%" . $datos[0] . "%' or apellido like '%" . $datos[1] . "%' or (nombre like '%" . $datos[0] . "%' and apellido like '%" . $datos[1] . "%');");
}
    if($query->rowCount()==0){
        $this->query="SELECT * FROM Usuarios order by 1 LIMIT 25;";
        $query =$this->$db->connect()->query("SELECT * FROM Usuarios order by 1 LIMIT 25;");
    }
}
    return $query;
}

function abrirFichaUsuario($idUsuario){

    $query =$this->$db->connect()->query("SELECT * FROM Usuarios where idUsuario='" . $idUsuario . "';");
    return $query;
}
function modificarUsuario($camp,$valor){

    $query = $this->$db->connect()->query("UPDATE Usuarios SET " . $camp . "='" . addslashes($valor) . "' WHERE idUsuario='" . $this->idUsuario . "';");
       return $query;

}
function insertarUsuario(){
    $password=hash('sha256','password');


    $db=$this->$db->connect();
    $date = date("Y-m-d H:i:s");
    $query =$db->prepare("INSERT INTO Usuarios (nombre,apellido,DNI,telefono,rol,activo,fechaCreacion,contrasena) VALUES(:nombre,:apellido,:dni,:telefono,:rol,1,'".$date."',:contrasena);");
    $query->bindParam(":nombre",addslashes($this->nombre));
    $query->bindParam(":apellido",addslashes($this->apellido) );
    $query->bindParam(":dni",addslashes($this->DNI));
    $query->bindParam(":telefono", addslashes($this->telefono) );
    $query->bindParam(":rol",addslashes($this->rol) );
    $query->bindParam(":contrasena",addslashes($password) );
    $db->beginTransaction();
 
    $query->execute();
  
    $id=$db->lastInsertId();
        $db->commit();  
  
        return $id;
}
function eliminarUsuario(){


    $query=$this->$db->connect()->query("SELECT * FROM Usuarios where idUsuario='". $this->idUsuario ."';");
    $fila=$query->fetchObject();
    if($fila->activo){
        $query = $this->$db->connect()->query("UPDATE Usuarios set Activo=0  where idUsuario='" . $this->idUsuario . "';");
    }else{
        $query = $this->$db->connect()->query("UPDATE Usuarios set Activo=1  where idUsuario='" . $this->idUsuario . "';");
    }
return $query;
}
function citaUsusario($idUsuario,$fecha){

    $query =$this->$db->connect()->query('INSERT INTO `Fichajes` (horaFichaje,idUsuario,tipo) values ("'.$fecha.'","'.$idUsuario.'","Cita")');
  
    return $query;
}

function obtenerHorario($idUsuario,$horario){

    if($horario=="fichajes"){
    $fecha1=date('Y-m-d', strtotime('-1 week'));
    $fecha2=date('Y-m-d', strtotime('+1 day'));
    $query =$this->$db->connect()->query('SELECT * FROM Fichajes where idUsuario="'.$idUsuario.'" and horaFichaje >= "'.$fecha1.'" and horaFichaje <= "'.$fecha2.'" order by horaFichaje asc ;');
    }
    if($horario=="citas"){
        $fecha1=date('Y-m-d', strtotime('+1 week'));
        $fecha2=date('Y-m-d', strtotime('+1 day'));
        $query =$this->$db->connect()->query('SELECT * FROM Fichajes where idUsuario="'.$idUsuario.'" and horaFichaje <= "'.$fecha1.'" and horaFichaje >= "'.$fecha2.'" order by horaFichaje desc ;');
    }

    return $query;
}
function obtenerCitasFiltrado($idUsuario,$fecha1,$fecha2){

    $query =$this->$db->connect()->query('SELECT * FROM Fichajes where idUsuario="'.$idUsuario.'" and horaFichaje >= "'.$fecha1.' 00:00:00" and horaFichaje <= "'.$fecha2.' 23:59:59" order by horaFichaje asc ;');
  
    return $query;

}
function eliminarCita($idCita){
  

    $query =$this->$db->connect()->query('DELETE FROM `Fichajes` where idFichaje="'.$idCita.'"');
  
    return $query;
}
function resetearPassword($password,$password2){
    if($password && $password2){
        $password=hash('sha256',$password);
        $query=$this->$db->connect()->query('SELECT * FROM `Usuarios` where idUsuario="'.$this->idUsuario.'" and contrasena= "'.$password.'"');
        $fila=$query->fetchObject();
        if($query->rowCount()==0){
            return false;
        }else{
            $password2=hash('sha256',$password2);
            $query=$this->$db->connect()->query('UPDATE Usuarios SET contrasena= "'.$password2.'" where  idUsuario="'. $this->idUsuario .'";');
        }
        return $query;
    }else{
        $password=hash('sha256',"password");
        $query=$this->$db->connect()->query('UPDATE Usuarios SET contrasena= "'.$password.'" where  idUsuario="'. $this->idUsuario .'";');
        return $query;
    }
   
}

}

?>