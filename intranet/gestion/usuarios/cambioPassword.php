<?php
session_start();
if(!isset($_SESSION['user_id'])){
header('Location: login.php');
exit;
} else {
// Show users the page!
}
include_once 'usuario.class.php';
?>
<?php
$usuario = new Usuario();?>
<script type="text/javascript" src="js/funcionesEstructura.js"></script>
<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

$idUsuario=$_REQUEST['idUsuario'];
?>
<div>
<div class="header d-flex justify-content-between m-2">
            <h3 style="text-transform:uppercase;"  >Cambio contraseña</h3>
            <button class="buttonCerrar" onclick="cierraCapa('ventana3')"><i class="fas fa-times"></i>Cerrar</button>
            </div>
            <div class="body">

            <form>
<div class="row">
    
    <div class="form-group m-1 col-12">
    <label>Contraseña vieja:</label>
    <div class="form-group m-1">
      <div class="input-group">
        <input type="password" class="form-control" id="oldPassword" required>
        <div class="input-group-append">
        <a class="input-group-text" onClick='mostrarContrasenaVieja()'><i id="old" class="fas fa-eye fa-lg"></i></a>
        </div>
      </div>
    </div>
    
      <div class="form-group m-1 col-12">
    <label>Contraseña nueva:</label>

    <div class="form-group m-1">
      <div class="input-group">
        <input type="password" class="form-control" id="newPassword" required>
        <div class="input-group-append">
        <a class="input-group-text" onClick='mostrarContrasenaNueva()' ><i id="new" class="fas fa-eye fa-lg"></i></a>
        </div>
      </div>

    </div>
      <div class="d-flex justify-content-end m-1 col-12" style="margin-top:20px !important;">
      <button type="button" class="btn btn-outline-primary" onclick="pantallaCarga();cambiarPassword();pantallaCarga();">Cambiar contrseña</button> 
      </div>
      
      </div>
</form>

            </div>

</div>
<script>


function mostrarContrasenaVieja() {
  let x = document.getElementById("old");
  let z = document.getElementById("oldPassword");

  if (z.type === "password") {
    z.type = "text";
    x.classList.remove('fa-eye');
    x.classList.add('fa-eye-slash');
  } else {
    z.type = "password";
    x.classList.remove('fa-eye-slash');
    x.classList.add('fa-eye');
  }
}

function mostrarContrasenaNueva() {
  let x = document.getElementById("new");
  let z = document.getElementById("newPassword");
  if (z.type === "password") {
    z.type = "text";
    x.classList.remove('fa-eye');
    x.classList.add('fa-eye-slash');
  } else {
    z.type = "password";
    x.classList.remove('fa-eye-slash');
    x.classList.add('fa-eye');
  }
}

function cambiarPassword(){
 if(!$('#oldPassword').val() || !$('#newPassword').val()){
  Swal.fire({
                      title: 'No puedes dejar los campos vacios',
                      icon: 'error',
                    });
 }else{
 $('#ventanaCarga').load('gestion/usuarios/usuariosOperaciones.php',{
            idUsuario:"<?=$idUsuario?>",
            oldPassword:$('#oldPassword').val(),
            newPassword:$('#newPassword').val(),
            tipo:"cambio",
            operacio:"cambiarPassword"
        }, (resultat) =>{ if(resultat=="1"){
        Swal.fire({
                      title: 'Contraseña cambiada',
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 1500
                    });
                    $("#ventana3").hide();
      }
        if(resultat==0){
         Swal.fire({
                      title: 'Los datos son incorrectos,no se pudo cambiar la contraseña',
                      icon: 'error',
                    });
        }}
        
        );
       }
}
</script>