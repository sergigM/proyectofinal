<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
include_once 'apiClientes.php';

$api = new Api();
if(isset($_REQUEST['dni']) && isset($_REQUEST['password']) && isset($_REQUEST['newPassword'])){
    $api->cambioContrasena($_REQUEST['dni'],$_REQUEST['password'],$_REQUEST['newPassword']);
  }else{
    $api->error('Error al llamar a a la api');
  }

?>