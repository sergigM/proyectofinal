<?php

include_once 'Cliente.php';

/*
$api = new Api();
if($_SERVER['REQUEST_METHOD']=='GET'){
  $api->getAll();
}
if($_SERVER['REQUEST_METHOD']=='POST'){
  if(isset($_POST['titulo']) && isset($_POST['idDestinatario'])){
    print_r($_POST['idDestinatario']);
    print_r($_POST['titulo']);
    
   $titulo=$_POST['titulo'];
   $idTascaDestinatari=$_POST['idDestinatario'];
    
    $api->add($titulo,$idTascaDestinatari);
  }else{
    $api->error('Error al llamar a a la api');
  }
}*/
class Api{

function getUsuarios(){
  $cliente= new Cliente();
  $clientes=array();
  $clientes["items"]=array();

  $res= $cliente->obtenerUsuarios();

  if($res->rowCount()){
    while($row=$res->fetch(PDO::FETCH_ASSOC)){
      $item= array(
       'id'=>$row['idUsuario'],
       'nombre'=>$row['nombre'],
       'apellido'=>$row['apellido'],
       'DNI'=>$row['DNI'],
       'email'=>$row['email'],
       'activo'=>$row['activo'],
       'rol'=>$row['rol']
      );
      array_push($clientes['items'],$item);
    }

  header('Content-Type: application/json');
    echo json_encode($clientes);
  }else{
   echo json_encode(array('mensaje'=>'No hay elementos registrados'));
  }
}


function getInfoFichajeQR($idUsuario){
  $cliente= new Cliente();
  $clientes=array();
  $clientes["items"]=array();

  $res= $cliente->obtenerEmpleados($idUsuario);

  if($res->rowCount()){
    while($row=$res->fetch(PDO::FETCH_ASSOC)){
      $item= array(
       'id'=>$row['idUsuario'],
       'nombre'=>$row['nombre'],
       'apellido'=>$row['apellido'],
       'DNI'=>$row['DNI'],
       'email'=>$row['email'],
       'activo'=>$row['activo'],
       'rol'=>$row['rol']
      );
      array_push($clientes['items'],$item);
    }

  header('Content-Type: application/json');
    echo json_encode($clientes);
  }else{
   echo json_encode(array('mensaje'=>'No hay elementos registrados'));
  }
  if($cliente->ficharEmpleado($idUsuario)){
    //header('Content-Type: application/json');
    echo "Oke";
  }else{
    echo "No oke";
  }
  
}

function getUsuariosFiltrado($dni,$password){
  /*
  $method = $_SERVER['REQUEST_METHOD'];
  if($method == "OPTIONS") {
      die();
  }*/
  
  $cliente = new Cliente();
  $clientes=array();
    $clientes["items"]=array();

  if($dni && $password){

    $res=$cliente->obtenerUsuarioFiltrado($dni,$password);
    if($res==false){
      echo json_encode(array('mensaje'=>'El DNI o la contrasena es incorrecto'));
    }
    if($res->rowCount()){
      while($row=$res->fetch(PDO::FETCH_ASSOC)){
        $item= array(
        'id'=>$row['idUsuario'],
        'nombre'=>$row['nombre'],
        'apellido'=>$row['apellido'],
        'DNI'=>$row['DNI'],
        'email'=>$row['email'],
        'telefono'=>$row['telefono'],
        'foto'=>$row['foto'],
        'activo'=>$row['activo'],
        'fechaCreacion'=>$row['fechaCreacion'],
        'horaFichaje'=>$row['horaFichaje'],
        'rol'=>$row['rol'],
        'tipo'=>$row['tipo']

        );
        array_push($clientes['items'],$item);
      }

  header('Content-Type: application/json');
      echo json_encode($clientes);
    }else{
    echo json_encode(array('mensaje'=>'No hay elementos registrados'));
    }
  }
}


function getCitasDia(){
  $cliente= new Cliente();
  $clientes=array();
  $clientes["listaCitas"]=array();

  $res=$cliente->obtenerCitasDia();

  if($res->rowCount()){
    while($row=$res->fetch(PDO::FETCH_ASSOC)){
      $listaCitas= array(
       'idFichaje'=>$row['idFichaje'],
       'horaFichaje'=>$row['horaFichaje'],
       'idUsuario'=>$row['idUsuario'],
       'tipo'=>$row['tipo']
      );
      array_push($clientes['listaCitas'],$listaCitas);
    }

  header('Content-Type: application/json');
    echo json_encode($clientes);
  }else{
   echo json_encode(array('mensaje'=>'No hay elementos registrados'));
  }
}









function error($mensaje){

  header('Content-Type: application/json');
 echo json_encode(array('mensaje'=>$mensaje));
}
function exito($mensaje){

  header('Content-Type: application/json');
 echo json_encode(array('mensaje'=>$mensaje));
}

function cambioContrasena($dni,$password,$newPassword){
  /*
  $method = $_SERVER['REQUEST_METHOD'];
  if($method == "OPTIONS") {
      die();
  }*/
  
  $cliente = new Cliente();

  if($dni && $password && $newPassword){

    $res=$cliente->cambioContrasena($dni,$password,$newPassword);
    if($res==false){
      echo json_encode(array('mensaje'=>'El DNI o la contrasena es incorrecto'));
    }
    if($res->rowCount()){
      echo json_encode(array('mensaje'=>'Si'));


    
    }else{
    echo json_encode(array('mensaje'=>'No hay elementos registrados'));
    }
  }
}




/*
function add($titulo,$idTascaDestinatari){
$tasques= new Tasca($db);
$camp='flagLlegitTascaDestinatari';
$valor=1;
if ($tasques->set($camp, $valor) && $tasques->set('idTascaDestinatari', $idTascaDestinatari)) {
if ($tasques->desaTasquesDestinatari()) {
       $jsonResultat['estatOperacio'] = true;
   } else {
       $jsonResultat['msgError'] = 'Error ' . $tasques->get_msgError();
       $jsonResultat['query'] = $tasques->get_sql_query();
   }
 } else {
   $jsonResultat['msgError'] = "falten camps o algun camp es incorrecte " . $tasques->get_sql_query();
 }

 $this->exito('Nuevo producto anadido');
}
}
*/
}
?>