<?php
include_once 'config.php';

class Cliente extends DB{
    private $dni;

    public function getDNI(){
        return $this->DNI;
    }
    
    public function setDNI($DNI){
        $this->DNI = $DNI;
    }

 function obtenerUsuarios(){
  $query =$this->connect()->query('SELECT * FROM `Usuarios` where rol="Cliente" ORDER BY `idUsuario` DESC');

  return $query;
 } 

 function obtenerEmpleados($idUsuario){
    $query =$this->connect()->query('SELECT * FROM `Usuarios` where `idUsuario`= "'.$idUsuario.'"');
  
    return $query;
   } 

function ficharEmpleado($idUsuario){
    $date = date("Y-m-d H:i:s");

    $query =$this->connect()->query('INSERT INTO `Fichajes` (horaFichaje,idUsuario,tipo) values ("'.$date.'","'.$idUsuario.'","Fichaje")');
  
    return $query;
}

 function obtenerUsuarioFiltrado($dni,$password){
     $contrasena=hash('sha256',$password);
     $query=$this->connect()->query('SELECT * FROM `Usuarios` where DNI="'.$dni.'" and contrasena= "'.$contrasena.'"');
     if($query->rowCount()==0){
         return false;
     }else{
        $query =$this->connect()->query('SELECT * FROM `Usuarios` INNER join `Fichajes` on Usuarios.idUsuario= Fichajes.idUsuario where DNI="'. $dni .'" and activo=1 ORDER BY horaFichaje DESC');
    
        if($query->rowCount()==0){
            $query =$this->connect()->query('SELECT Usuarios.* FROM `Usuarios` LEFT join `Fichajes` on Usuarios.idUsuario= Fichajes.idUsuario where  DNI="'. $dni .'" and activo=1 ORDER BY horaFichaje DESC');
        }
     }
    return $query;
 }
 function cambioContrasena($dni,$password,$newPassword){
     $password=hash('sha256',$password);
    $query=$this->connect()->query('SELECT * FROM `Usuarios` where DNI="'.$dni.'" and contrasena= "'.$password.'"');
    $fila=$query->fetchObject();
    if($query->rowCount()==0){
        return false;
    }else{
        $newPassword=hash('sha256',$newPassword);
        $query=$this->connect()->query('UPDATE Usuarios SET contrasena= "'.$newPassword.'" where  DNI="'. $dni .'";');
    }
    return $query;

 }

 function obtenerCitasDia(){
   $date = date("Y-m-d");
 
   $query=$this->connect()->query('SELECT * FROM `Fichajes` where horaFichaje > "'.$date.' 00:00:00" and horaFichaje < "'.$date.' 23:59:59";');
  
   return $query;

}
 
}

?>
