import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';
import QRCode from 'qrcode';
import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../popover/popover.component';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {
  ocultar: boolean = false;
  esconder: boolean = true;
  body: any;
  citas: any;
  items: any;
  fichaje: any = [];
  cita: any = [];
  citaDia: any = [];

  code: any;
  generated = '';

  displayQrCode() {
    return this.generated !== '';
  }

  constructor(private data: DataService, private popover: PopoverController) {
    this.citas = this.data.getCitas();
    this.body = this.data.getBody();
    this.code = this.body.items[0].id;
    console.log('citas');
    if (this.body.items[0].horaFichaje) {
      this.filtrarHora();
      console.log('e');
    }
  }

  async presentPopover(ev: any) {
    const popOver = await this.popover.create({
      component: PopoverComponent,
      cssClass: 'my-popover-class',
      event: ev,
    });

    popOver.onDidDismiss().then((data) => console.log(data));

    return await popOver.present();
  }

  filtrarHora() {
    for (let a = 0; a < this.body.items.length; a++) {
      let dividir = { dia: '', hora: '' };
      let division = this.body.items[a].horaFichaje.split(' ');

      if (this.body.items[a].tipo == 'Cita') {
        dividir = { dia: division[0], hora: division[1] };
        this.cita.push(dividir);
      } else if (this.body.items[a].tipo == 'Fichaje') {
        dividir = { dia: division[0], hora: division[1] };
        this.fichaje.push(dividir);
      }
    }
  }

  filtrarHoraCita() {
    for (let a = 0; a < this.citas.listaCitas.length; a++) {
      let dividir = { dia: '', hora: '' };
      let division = this.citas.listaCitas[a].horaFichaje.split(' ');
      dividir = { dia: division[0], hora: division[1] };
      this.citaDia.push(dividir);
    }
  }

  hidecita() {
    console.log(this.cita);
    if (this.esconder === true) {
      this.esconder = !this.esconder;
      document.getElementById('citas_tabla').hidden = false;
    } else if (this.esconder === false) {
      this.esconder = !this.esconder;
      document.getElementById('citas_tabla').hidden = true;
    }
  }

  hidefichaje() {
    console.log(this.fichaje);
    if (this.esconder === true) {
      this.esconder = !this.esconder;
      document.getElementById('fichajes_tabla').hidden = false;
    } else if (this.esconder === false) {
      this.esconder = !this.esconder;
      document.getElementById('fichajes_tabla').hidden = true;
    }
  }

  process() {
    var a = document.getElementById('a').hidden;

    if (this.ocultar === true) {
      this.ocultar = !this.ocultar;
      document.getElementById('a').hidden = false;
      console.log(a);
    } else {
      this.ocultar = !this.ocultar;
      document.getElementById('a').hidden = false;
      console.log(a);
    }
  }

  ngOnInit() {
    const qrcode = QRCode;
    const self = this;
    qrcode.toDataURL(
      self.code,
      { errorCorrectionLevel: 'H' },
      function (err, url) {
        self.generated = url;
      }
    );
  }
}
