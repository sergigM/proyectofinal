import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  body: any;


  constructor(private router: Router, private data: DataService) { 
    this.body = this.data.getBody();

  }

  ngOnInit() {}

 

  logout(){
    this.router.navigateByUrl('/', { replaceUrl:true });
  }

  password(){
    this.router.navigate(['password'])
  }

}
