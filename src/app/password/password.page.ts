import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {

  dni: string;
  password : string;
  newpassword : string;
  confirma : string;
  body: any;
  showPassword = false;
  passwordToggleIcon = 'eye';

  constructor(private data: DataService,private router: Router,public alertController: AlertController,private popoverController: PopoverController) {
    this.body = this.data.getBody();
    this.dni = this.body.items[0].DNI;
   }

  ngOnInit() {
    this.DismissClick();
  }

 

  async alertaCoincidir() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'ERROR',
      subHeader: 'Error al enviar',
      message: 'Rellena todos los campos y haz que las contraseñas coincidan',
      buttons: ['OK']
    });
  
    await alert.present();
  
  }
  

  togglePassword(){
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon == 'eye'){
      this.passwordToggleIcon = 'eye-off';
    }else{
      this.passwordToggleIcon = 'eye';
    }
  }

  async consulta(){

    if(this.confirma == this.newpassword && this.password && this.newpassword && this.confirma){

      const info = await fetch(`https://carlos.lluert.dev/apiProyecto/cambiarContrasena.php?dni=${this.dni}&password=${this.password}&newPassword=${this.newpassword}`)
      this.body= await info.json();
      console.log('loguea')
      console.log(this.body);
      this.data.setBody(this.body);
      this.router.navigateByUrl('/', { replaceUrl:true });

    }
    else{
      this.alertaCoincidir();
    }
   


  }

  async DismissClick() {
    await this.popoverController.dismiss();
    }

  async changePassword(){
    const info = this.consulta();
    info;
    
  }
  

}
