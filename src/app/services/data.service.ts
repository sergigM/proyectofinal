import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  body: any;
  citas : any;

  constructor() { }

  setBody(info){
    console.log(info);
    this.body = info;
  }

  getBody(){
    console.log(this.body);
    return this.body; 
  }

  setCitas(citas){
    this.citas = citas;

  }

  getCitas(){
    console.log(this.citas);
    return this.citas;
  }
 
}
