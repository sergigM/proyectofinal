import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { AlertController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';


@Component({
selector: 'app-login',
templateUrl: './login.page.html',
styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

  dni: string;
  password : string;
  body: any;
  citas: any;
  showPassword = false;
  passwordToggleIcon = 'eye';
 


constructor(private router: Router, private data: DataService, public alertController: AlertController,private popoverController: PopoverController) {}

ngOnInit() {
}

ionViewWillEnter(){
  this.DismissClick();
}

togglePassword(){
  this.showPassword = !this.showPassword;

  if(this.passwordToggleIcon == 'eye'){
    this.passwordToggleIcon = 'eye-off';
  }else{
    this.passwordToggleIcon = 'eye';
  }
}

async presentAlert() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'ERROR',
    subHeader: 'Campos vacios',
    message: 'Introduce la información solicitada',
    buttons: ['OK']
  });

  await alert.present();

  const { role } = await alert.onDidDismiss();
  console.log('onDidDismiss resolved with role', role);
}


async alerta() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'ERROR',
    subHeader: 'Error de verificación',
    message: 'Introduce correctamente tu DNI o contraseña',
    buttons: ['OK']
  });

  await alert.present();

  const { role } = await alert.onDidDismiss();
  console.log('onDidDismiss resolved with role', role);
}

async getCitas(){
  const citas = await fetch(`https://carlos.lluert.dev/apiProyecto/obtenerCitasDia.php`)
  this.citas = await citas.json();
  this.data.setCitas(this.citas);
  console.log(this.citas.listaCitas);

}


async loguea(){

  const info = await fetch(`https://carlos.lluert.dev/apiProyecto/busquedaUsuariosFiltrado.php?dni=${this.dni}&password=${this.password}`)
  this.body= await info.json();
  console.log('loguea')
  console.log(this.body);
  this.data.setBody(this.body);
  if(!this.body.items){
    this.alerta();
  }else if(!this.dni || !this.password){
    this.presentAlert();

  }else{
    this.router.navigate(['/usuario']);

  }

  }

log() {
    const info = this.loguea();
    const citas = this.getCitas();
    citas;
    info;
  
}

async DismissClick() {
  await this.popoverController.dismiss();
  }

}
