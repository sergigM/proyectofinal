export interface Usuario {
    id: string;
    nombre: string;
    apellido: string;
    telefono: number;
    email: string;
}
